<?php
# Direct Access is not allowed
if (eregi("header.php",$_SERVER['PHP_SELF'])) {
    Header("Location: ../index.php");
    die();
}

# Include user config, branding, metadata, stylesheets and icons
require_once("config_inc.php");

include("branding_inc.php");

$stylesheetsrc = $nav_path . "/images/stylesheet.css";
$iphonesheetsrc = $nav_path . "/images/iphone.css";
$meta = $app_path . "/includes/meta.php";

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

	<title><?php echo $application_name; ?></title>
	
	<?php include($meta); ?>
	
	<link rel="stylesheet" href="<?php echo $stylesheetsrc; ?>" type="text/css" />
	<link rel="shortcut icon" href="<?php echo $shortcut; ?>" type="image/x-icon" />
	
	<link media="only screen and (max-device-width: 480px)" rel="stylesheet" type="text/css" href="<?php echo $iphonesheetsrc; ?>"/>

	<meta name="viewport" content="width = 680">
	
<?
if($section_type=="admin")
 echo '	<script type="application/x-javascript">
		window.onload = function() {setTimeout(function(){window.scrollTo(0, 1);}, 100);}
	</script>';
?>
	<!-- Thickbox (modified version to work with prototype) : !!! needs to be loaded before prototype to fix $() issue with prototype -->



	
	<!-- prototype and scriptaculous -->
	<script src="<?php echo($nav_path) ?>/js/prototype.js" type="text/javascript"></script>
	<script src="<?php echo($nav_path) ?>/js/scriptaculous.js" type="text/javascript"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script>
jQuery.noConflict();
</script>

	<script type="text/javascript" src="<?php echo($nav_path) ?>/js/thickbox/thickbox.js"></script>
	<link rel="stylesheet" href="<?php echo($nav_path) ?>/js/thickbox/thickbox.css" type="text/css" media="screen" />	
	<!-- own javascripts -->
	<script src="<?php echo($nav_path) ?>/includes/help.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo($nav_path) ?>/js/info/css/info.css" type="text/css" />';

</head>

<body>
	<div id="wrapper<?if($section_type=="admin") echo "admin";?>" <?if($section_type!="admin" && $settings[site_width]!="") print 'style="width:'.$settings[site_width].'"';?>><center><img src="<?php echo $logo_lg; ?>"><div class="toptext"><?=$top_text;?></div></center><br>
		<a href="<?php echo $nav_path . '/index.php' ?>"> <?php echo _HOME; ?> </a> | 
		<a href="<?php echo $nav_path . '/faq.php' ?>"> <?php echo _FAQ; ?> </a> |
		<a href="<?php echo $nav_path . '/admin/index.php' ?>"> <?php echo _ADMIN; ?> </a> 
