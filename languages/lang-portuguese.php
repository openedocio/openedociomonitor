<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","ISO-8859-1");
define("_HOME"," Inicio ");
define("_FAQ","Perguntas");
define("_ABOUT","Sobre");
define("_ADMIN","Admin");
define("_AREASEXPERTISE","Areas de Conhecimento");
define("_SPONSORS","Patrocinadores");
define("_FOLLOWUP","Itens para fazer acompanhamento");
define("_FAQLONG","Perguntas Frequentes");
define("_QUESTION","Intravista");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">Voltar</a> ]");
define("_FAQ_TEXT","<h1>Doing an Edocio</h1><p>Read each page or screen as presented to you. Then select from the 5 sentences presented. Select the one contained in the page or screen you just read. <br /><br /><b>Take care to select the correct sentence because choosing a sentence not contained in the page you just read will send you one or more back not forward. </b></p>");
define("_HEADER_TITLE","<h2>The Role of Philosophy<br>in the Evolution of Western Culture</h2><h3>By Wallace H Provost Jr.</h3>");

define("_INPUT_FINISH_CODE","Input a finish code from a previous unit");
define("_YOUR_CODE_WRONG","Your code is wrong!");

define("_PLAY_AUDIO","Play the Audio");
define("_PAUSE_AUDIO","Pause the Audio");
define("_INCREASE_VOLUME","Increase Volume");
define("_DECREASE_VOLUME","Decrease Volume");

# Admin Text
define("_NEWEXPERTISE","Criar uma entrevista nova");
define("_ADMINHOME","Administração");
define("_USERS","Usuários");
define("_SETTINGS","Definições");
define("_ADD","Adicionar");
define("_SAVECHANGES","Salvar alterações");
define("_CARRYFORWARD","Reporte");
define("_NEXTLINK","Próximo Link");
define("_DELETE","Excluir");
define("_NEWQUESTION","Nova Pergunta");
define("_NEWANSWER","Nova Resposta");
define("_LINKTO","Link Para");
define("_CONFIRMDELETE", "Tem certeza de que deseja apagar esta entrevista?");
define("_ADMINLOGIN", "Administração Login");
define("_USERNAME", "Nome de usuário");
define("_PASSWORD", "Senha");
define("_PASSWORDERROR", "O nome de usuário ea senha que você forneceu não correspondem.");
define("_PROPERTIES","Propriedades");
define("_HIDE","Esconder");
define("_SHOW","Mostrar");
define("_REALLYDELETE","Realmente Excluir?");
define("_EXPORT","Export");
define("_IMPORT","Importar Entrevistas");
define("_DOWNLOADFILE","Baixe-o como um arquivo de exportação");

# Help Text
define("_NO_HELP_TEXT","Sem ajuda disponível");
define("_ADVANCED_HELP_TEXT","Clique para obter ajuda");
define("_NEED_HELP","Precisa de Ajuda?");
define("_MORE_TOOLS","Mais ferramentas");
define("_HIDE_HELP","Ocultar ajudar");
define("_NO_HELP_AVAILABLE","Sem ajuda disponível para este item");
define("_DEFAULT_ADV_HELP_TEXT","Passe o mouse sobre uma resposta para obter ajuda sobre isso");
define("_DEFAULT_ADV_NO_HELP_TEXT","Sem ajuda está disponível para esta resposta");

?>