<?php
# include selected language
include_once("../../languages/lang-french.php");

# include versionning information
require_once( '../includes/version.php' );

if(isset($_POST[finish]))
 {
	chmod("../../installation", 000);
	header("Location: ../../");
	exit();
 }
?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Open Expert [<?php echo($_VERSION->_RELEASE) ?>] Web Installer</title>
	
	<meta http-equiv="content-style-type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../style/install.css" media="screen" />
	<link rel="shortcut icon" href="../style/img/openexpert.ico" type="image/x-icon" />
	
	<!-- prototype and scriptaculous -->
	<script src="../../js/prototype.js" type="text/javascript"></script>
	<script src="../../js/scriptaculous.js" type="text/javascript"></script>
	
</head>
<body>

<div id="global">
	<div id="entete">
		<h1>Welcome to OpenExpert Installation !!!</h1>
		<br />
		<p class="sous-titre">
			<img alt="" src="../style/img/openexpert-logo.gif" />
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><h3 style="padding-top: 10px">You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3><![endif]-->
			<comment><h3>You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3></comment>
			
		</p>
		<p style="clear: both;">
			<h3><strong>Follow the steps bellow to complete installation :</strong></h3>
		</p>
	</div>

	<div id="centre">
		<div id="navigation">
			<div>
				<h3>Licence</h3>
			</div>
			<div>
				<h3>Permissions</h3>
			</div>
			<div>
				<h3>Database Settings</h3>
			</div>
			<div>
				<h3>Application Settings</h3>
			</div>
			<div>
				<h3>Branding</h3>
			</div>
			<div>
				<h2><u>Installation complete</u></h2>
			</div>
		</div>

		<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
		<!--[if IE]><div id="contenu" style="margin-top: -10px; padding-top: 0px;" ><![endif]-->
		<comment><div id="contenu"></comment>
			<h2 style="text-align: center;">Installation complete !</h2>
			<h3>Congratulations, the installation process ended successfully.</h3>
			<ul>
			<li style="color:green;"><strong><span style="color:green;">Uploading tables in database : OK</span></strong></li>
			<li style="color:green;"><strong><span style="color:green;">Setting application paths : OK</span></strong></li>
			<li style="color:green;"><strong><span style="color:green;">Setting admin login/password : OK</span></strong></li>
			<li style="color:green;"><strong><span style="color:green;">Branding of application : OK</span></strong></li>
			</ul>
			<h3><u>Note :</u> The installation directory won't be deleted from your server, but you'll not be taken to it anymore unless you erase 
			/noncore/config_inc.php (so, if something bad happens to your site, you know how to get back to a clean install).</h3>
			<br />
			<h2 style="text-align: center;">You can now go to <a href="../../index.php" target="_blank">your site</a>.</h2>
			<div id="nav-bottom">
				<form action="install6.php" style="text-align: center;" method="POST">
					<div id="nav-bottom-left"><a href="install5.php">&lt; Back to Branding</a></div>
					<div id="nav-bottom-right"><input type="submit" name="finish" value="Finish"  /></div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="pied">
		<p><?php echo($_VERSION->_URL) ?></p>
		<p><?php echo($_VERSION->_COPYRIGHT) ?></p>
	</div>
</div>

</body>
</html>