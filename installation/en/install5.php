<?php
# include selected language
include_once("../../languages/lang-french.php");

# include versionning information
require_once( '../includes/version.php' );

require_once( '../../includes/utils.php' );

# media directory path
$imgPath = '../../noncore/images/';

$allowed_langs = allowed_languages();

# flags
$appName = $_POST["appName"];
$logoName = $_POST["logoName"];
$iconName = $_POST["iconName"];
$logoUpload = $_POST["logoUpload"];
$iconUpload = $_POST["iconUpload"];
$siteWidth = $_POST["siteWidth"];
$uploadError = '';

# default field values if needed
if ($appName == null) $appName = 'OpenExpert.org';
if ($logoName == null) $logoName = 'openexpert-logo.gif';
if ($iconName == null) $iconName = 'openexpert.ico';
if ($logoUpload == null) $logoUpload = 0;
if ($iconUpload == null) $iconUpload = 0;
if ($siteWidth == null) $siteWidth = '100%';

# if logo upload, handle it
if ($logoUpload == 1){
	
    $content_dir = $imgPath;
    $tmp_file = $_FILES['file']['tmp_name'];

    if(!is_uploaded_file($tmp_file)){
        $uploadError = "Unable to find file";
    } else {
	    // check file extension
	    $file_type = $_FILES['file']['type'];

	    if(!strstr($file_type, 'jpg') && !strstr($file_type, 'jpeg') && !strstr($file_type, 'bmp') && !strstr($file_type, 'gif')
			&& !strstr($file_type, 'jpeg') && !strstr($file_type, 'png')){
	        $uploadError = "File is not an image";
	    } else {
		    // copy file to dest folder (test that there are no invalid char in its name)
		    $file_name = $_FILES['file']['name'];

			if( preg_match('#[\x00-\x1F\x7F-\x9F/\\\\]#', $file_name) ){
			    $uploadError = "File name is not valid";
			}
			else if(!move_uploaded_file($tmp_file, $content_dir . $file_name)){
		        $uploadError = "Unable to upload file in $content_dir";
		    } else {
				# chmod it to 777
				@chmod ($content_dir . $file_name, 0777);
			}

			# set flags
			$logoName = $file_name;
			$logoUpload = 0;
		
		}
	}
}

# if icon upload, handle it
if ($iconUpload == 1){
	
    $content_dir = $imgPath;
    $tmp_file = $_FILES['file']['tmp_name'];

    if(!is_uploaded_file($tmp_file)){
        $uploadError = "Unable to find file";
    } else {
	    // check file extension
		// for now, can only be .ico for complete IE compatibility. Should evolve in later releases to allow other files formats.
	    $file_type = $_FILES['file']['type'];

	    if(!strstr($file_type, 'ico')){
	        $uploadError = "File is not an icon. You can only select for now .ico files, for complete IE compatibility. This behavior will evolve in later releases to allow other files formats.";
	    } else {
		    // copy file to dest folder (test that there are no invalid char in its name)
		    $file_name = $_FILES['file']['name'];

			if( preg_match('#[\x00-\x1F\x7F-\x9F/\\\\]#', $file_name) ){
			    $uploadError = "File name is not valid";
			}
			else if(!move_uploaded_file($tmp_file, $content_dir . $file_name)){
		        $uploadError = "Unable to upload file in $content_dir";
		    } else {
				# chmod it to 777
				@chmod ($content_dir . $file_name, 0777);
			}

			# set flags
			$iconName = $file_name;
			$iconUpload = 0;
		
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>OpenExpert [<?php echo($_VERSION->_RELEASE) ?>] Web Installer</title>
	
	<meta http-equiv="content-style-type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../style/install.css" media="screen" />
	<link rel="shortcut icon" href="../style/img/openexpert.ico" type="image/x-icon" />
	
	<!-- prototype and scriptaculous -->
	<script src="../../js/prototype.js" type="text/javascript"></script>
	<script src="../../js/scriptaculous.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		function check() {
			// we check that the fields are correctly filled
			var paramValid=false;

			if ( $('appName').value == '' ) {
				alert("Please enter a name for your application.");
				$('appName').style.background="#ff9999";
				$('appName').focus();
				paramValid=false;
			} else if ( $('logoName').value == '' ) {
				alert('Please select a logo.');
				$('logoName').style.background="#ff9999";
				$('logoName').focus();
				paramValid=false;
			} else if ( $('iconName').value == '' ) {
				alert('Please select an icon.');
				$('iconName').style.background="#ff9999";
				$('iconName').focus();
				paramValid=false;
			} 
			else if ( $('siteWidth').value == '' ) {
				alert('Please input width.');
				$('siteWidth').style.background="#ff9999";
				$('siteWidth').focus();
				paramValid=false;
			}else {
				paramValid=true;

				// Write data, calling ajax subroutine
				var url = '../includes/branding.php';
				var help_text = $("help_text").checked ? $("help_text").value : '';
				var pars = 'appName=' + $('appName').value + '&topText=' + encodeURIComponent($('topText').value) + '&help_text=' + help_text + '&language=' + encodeURIComponent($('language').value) + '&logo=$nav_path/noncore/images/' + $('logoName').value + '&icon=$nav_path/noncore/images/' + $('iconName').value + '&width=' + encodeURIComponent($('siteWidth').value);

				new Ajax.Request(url, {
					method: 'get',
					parameters: pars,
					onSuccess: function(transport, json){
						if (transport.responseText == 1){
							// all is OK
							alert('Branding settings successfully writen to config file.\nYou can now continue !');
							$('save-btn').disabled = true;
							$('continue').disabled = false;
						} else {
							// problem while writing data
							alert('An error occured while writing branding settings to config file :\n' + transport.responseText + '\nPlease try again.');
						}
					}
				});
				
			}
			
			return paramValid;
		}
		
		function pre_ico_ul(){
			$('iconUpload').value = 1;
			$('hidden-ico-appName').value = $('appName').value;
			$('hidden-ico-logoName').value = $('logoName').value;
			return true;
		}
		
		function pre_lg_ul(){
			$('logoUpload').value = 1;
			$('hidden-lg-appName').value = $('appName').value;
			$('hidden-lg-iconName').value = $('iconName').value;
			return true;
		}
	</script>
	
</head>
<body>

<div id="global">
	<div id="entete">
		<h1>Welcome to OpenExpert Installation !!!</h1>
		<br />
		<p class="sous-titre">
			<img alt="" src="../style/img/openexpert-logo.gif" />
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><h3 style="padding-top: 10px">You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3><![endif]-->
			<comment><h3>You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3></comment>
			
		</p>
		<p style="clear: both;">
			<h3><strong>Follow the steps bellow to complete installation :</strong></h3>
		</p>
	</div>

	<div id="centre">
		<div id="navigation">
			<div>
				<h3>Licence</h3>
			</div>
			<div>
				<h3>Permissions</h3>
			</div>
			<div>
				<h3>Database Settings</h3>
			</div>
			<div>
				<h3>Application Settings</h3>
			</div>
			<div>
				<h2><u>Branding</u></h2>
			</div>
			<div>
				<h3>Installation complete</h3>
			</div>
		</div>

		<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
		<!--[if IE]><div id="contenu" style="margin-top: -10px; padding-top: 0px;" ><![endif]-->
			<div id="contenu">
			<h2 style="text-align: center;">Branding</h2>
			<h3>Register here your application name, logo and browser icon.</h3>
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><table><![endif]-->
			<table style="width:100%;">
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>
						<strong>Application name</strong>
						<br/>
						<input type="text" id="appName" style="width:250px;" value="<?php echo "$appName"; ?>" />
					</td>
					<td>
						<em>will appear in title bar of a browser</em>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Description at the top</strong>
						<br/>
						<textarea id="topText" style="width:250px;height:100px;"><?php echo "$topText"; ?></textarea>
					</td>
					<td>
						<em>will appear at the top of the page<br />HTML is allowed</em>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Site width</strong>
						<br/>
						<input type="text" id="siteWidth" value="<?php echo "$siteWidth"; ?>" />
					</td>
					<td>
						<em>for example: 70% or 800px</em>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Language</strong>
						<br/>
				<select name="language" id="language">
					<?php
					foreach ($allowed_langs as $lang => $langname) {
						echo "<option value='".$langname."'>".$langname."</option>";
					}
					?>
				</select>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Additional help</strong> <input type="checkbox" name="help_text" id="help_text" value="1" CHECKED> Enabled
						
				
					</td>
					<td>If enabled, you can show an additional help for questions
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3>
						<strong>Logo : </strong>actual is <strong><span style="color:green;"><u><?php echo "$logoName"; ?></u></span></strong>
						<input type="hidden" id="logoName" value="<?php echo "$logoName"; ?>" disabled />
					</td>
				</tr>
				<tr>
				<form method="post" enctype="multipart/form-data" action="install5.php" onsubmit="return pre_lg_ul();">
					<td colspan=3>
						<input type="file" name="file" size="30" />
						<input type="submit" name="upload" value="Upload" />
						<input type="hidden" id="logoUpload" name="logoUpload" value="<?php echo "$logoUpload"; ?>" />
						<input type="hidden" id="hidden-lg-appName" name="appName" value="<?php echo "$appName"; ?>" />
						<input type="hidden" id="hidden-lg-iconName" name="iconName" value="<?php echo "$iconName"; ?>" />
					</td>
				</form>
				</tr>
				<tr>
					<td colspan=3>
						<em>will appear in head of each page (default is openexpert-logo.gif).<br />The default size of the logo is 266 x 59 pixels.</em>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan=3>
						<strong>Icon : </strong>actual is <strong><span style="color:green;"><u><?php echo "$iconName"; ?></u></span></strong>
						<input type="hidden" id="iconName" value="<?php echo "$iconName"; ?>" disabled />
					</td>
				</tr>
				<tr>
				<form method="post" enctype="multipart/form-data" action="install5.php" onsubmit="return pre_ico_ul();">
					<td colspan=3>
						<input type="file" name="file" size="30" />
						<input type="submit" name="upload" value="Upload" />
						<input type="hidden" id="iconUpload" name="iconUpload" value="<?php echo "$iconUpload"; ?>" />
						<input type="hidden" id="hidden-ico-appName" name="appName" value="<?php echo "$appName"; ?>" />
						<input type="hidden" id="hidden-ico-logoName" name="logoName" value="<?php echo "$logoName"; ?>" />
					</td>
				</form>
				</tr>
				<tr>
					<td colspan=3>
						<em>will be displayed in browser navigation bar (default is openexpert.ico)</em>
					</td>
				</tr>
			</table>
			<br />
			<div style="text-align: center;">
				<input type="button" id="reset-btn" onClick="javascript:document.location='install5.php'" value="Initialize fields" />
				<input type="button" id="save-btn" onClick="check();" value="Save values" />
			</div>

			<div id="nav-bottom">
				<form action="install6.php" style="text-align: center;">
					<div id="nav-bottom-left"><a href="install4.php">&lt; Back to App settings</a></div>
					<div id="nav-bottom-right"><input type="submit" id="continue" value="Continue" disabled /></div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="pied">
		<p><?php echo($_VERSION->_URL) ?></p>
		<p><?php echo($_VERSION->_COPYRIGHT) ?></p>
	</div>
</div>

<script type="text/javascript">
	uploadError = "<?php echo($uploadError) ?>"

	// alert if error in upload
	if (uploadError != ""){
		alert(uploadError);
	}
</script>

</body>
</html>
