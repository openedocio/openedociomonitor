<?php
# include selected language
include_once("../../languages/lang-french.php");

# include versionning information
require_once( '../includes/version.php' );

# function to get current page URL
	/**
	* Get current page
	* @return string page
	*/
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
$curPageURL = curPageURL();

# default field values
$appPath = substr($_SERVER["SCRIPT_FILENAME"], 0, strrpos($_SERVER["SCRIPT_FILENAME"],"/installation/"));
$navPath = substr($curPageURL, 0, strrpos($curPageURL,"/installation/"));
$adminName = 'admin';
$adminPwd = 'password';

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Open Expert [<?php echo($_VERSION->_RELEASE) ?>] Web Installer</title>
	
	<meta http-equiv="content-style-type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../style/install.css" media="screen" />
	<link rel="shortcut icon" href="../style/img/openexpert.ico" type="image/x-icon" />
	
	<!-- prototype and scriptaculous -->
	<script src="../../js/prototype.js" type="text/javascript"></script>
	<script src="../../js/scriptaculous.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		function check() {
			// we check that the fields are correctly filled
			var paramValid=false;

			if ( $('appPath').value == '' ) {
				alert("Please enter the path to your server's root.");
				$('appPath').style.background="#ff9999";
				$('appPath').focus();
				paramValid=false;
			} else if ( $('navPath').value == '' ) {
				alert('Please enter the url of the web site.');
				$('navPath').style.background="#ff9999";
				$('navPath').focus();
				paramValid=false;
			} else if ( $('adminName').value == '' ) {
				alert('Please enter a user name for administration panel.');
				$('adminName').style.background="#ff9999";
				$('adminName').focus();
				paramValid=false;
			} else {
				paramValid=true;

				// Write data, calling ajax subroutine
				var url = '../includes/appSettings_inc.php';
				var pars = 'appPath=' + $('appPath').value + '&navPath=' + $('navPath').value + '&adminName=' + $('adminName').value + '&adminPwd=' + $('adminPwd').value;

				new Ajax.Request(url, {
					method: 'get',
					parameters: pars,
					onSuccess: function(transport, json){
						if (transport.responseText == 1){
							// all is OK
							alert('Application settings successfully writen to config file.\nYou can now continue !');
							$('save-btn').disabled = true;
							$('continue').disabled = false;
						} else {
							// problem while writing data
							alert('An error occured while writing application settings to config file :\n' + transport.responseText + '\nPlease try again.');
						}
					}
				});
				
			}
			
			return paramValid;
		}
	</script>
	
</head>
<body>

<div id="global">
	<div id="entete">
		<h1>Welcome to OpenExpert Installation !!!</h1>
		<br />
		<p class="sous-titre">
			<img alt="" src="../style/img/openexpert-logo.gif" />
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><h3 style="padding-top: 10px">You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3><![endif]-->
			<comment><h3>You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3></comment>
			
		</p>
		<p style="clear: both;">
			<h3><strong>Follow the steps bellow to complete installation :</strong></h3>
		</p>
	</div>

	<div id="centre">
		<div id="navigation">
			<div>
				<h3>Licence</h3>
			</div>
			<div>
				<h3>Permissions</h3>
			</div>
			<div>
				<h3>Database Settings</h3>
			</div>
			<div>
				<h2><u>Application Settings</u></h2>
			</div>
			<div>
				<h3>Branding</h3>
			</div>
			<div>
				<h3>Installation complete</h3>
			</div>
		</div>

		<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
		<!--[if IE]><div id="contenu" style="margin-top: -10px; padding-top: 0px;" ><![endif]-->
		<comment><div id="contenu"></comment>
			<h2 style="text-align: center;">Application Settings</h2>
			<h3>Fill the following fields to set application paths and login/password to enter administration panel. Click the 'Save values' button to write them to /noncore/config_inc.php.</h3>
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><table><![endif]-->
			<comment><table style="width:100%;"></comment>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><u>1) Application paths</u></td>
				</tr>
				<tr>
					<td colspan="3">
						<strong>Application path on the server</strong>						
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<input type="text" id="appPath" value="<?php echo "$appPath"; ?>" style="width: 100%" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<em>path to the root of your website on your web server.</em>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<strong>Site URL</strong>
					</td>
				</tr>
				<tr>
					<td colspan="3" width="100%">
						<input type="text" id="navPath" value="<?php echo "$navPath"; ?>" style="width: 100%" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<em>your website url</em>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3"><u>2) Access to administration panel</u></td>
				</tr>
				<tr>
					<td>
						<strong>Admin name</strong>
						<br/>
						<input type="text" id="adminName" value="<?php echo "$adminName"; ?>" />
					</td>
					<td>
						<em>user name to enter administration panel</em>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Admin password</strong>
						<br/>
						<input type="text" id="adminPwd" value="<?php echo "$adminPwd"; ?>" />
					</td>
					<td>
						<em>password to enter administration panel</em>
					</td>
				</tr>
			</table>
			<br />
			<div style="text-align: center;">
				<input type="button" id="reset-btn" onClick="javascript:document.location='install4.php'" value="Initialize fields" />
				<input type="button" id="save-btn" onClick="check();" value="Save values" />
			</div>

			<div id="nav-bottom">
				<form action="install5.php" style="text-align: center;">
					<div id="nav-bottom-left"><a href="install3.php">&lt; Back to DB settings</a></div>
					<div id="nav-bottom-right"><input type="submit" id="continue" value="Continue" disabled /></div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="pied">
		<p><?php echo($_VERSION->_URL) ?></p>
		<p><?php echo($_VERSION->_COPYRIGHT) ?></p>
	</div>
</div>

</body>
</html>
