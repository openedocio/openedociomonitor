<?
	/**
	* Show statistics - list of users.
	* @return string data
	*/
 function statistics()
 {
	global $mysql;
	global $title;

	$title = 'Statistics';

		if($_GET[page]=="") $page = 1; else $page = $_GET[page];

		$page_print = new page_view('monitor','50',$page,' ','','<span>%i%</span>');
		$page_print->type = "control";
		$page_print->full_query = "SELECT monitor.*,ExpertiseArea.area_text FROM monitor
LEFT JOIN ExpertiseArea ON ExpertiseArea.area_id=monitor.area_id
 GROUP BY monitor.guest_id, monitor.area_id";

		$page_print->count_numrows = true;

		$page_url = './main.php?statistics';

		$page_print->next_part = '<a href="'.$page_url.'&page=%i%">%name%</a>';
		$page_print->link = '<a href="'.$page_url.'&page=%i%">%i%</a>';

		$page_print->create();

		$page_print->prev_page('<a href="'.$page_url.'&page=%i%">Prev</a> ','<span>Prev</span> ');
		$page_print->next_page(' <a href="'.$page_url.'&page=%i%">Next</a>',' <span>Next</span>');


	$result = $mysql->Select("SELECT monitor.*,ExpertiseArea.area_text FROM monitor
LEFT JOIN ExpertiseArea ON ExpertiseArea.area_id=monitor.area_id
 GROUP BY monitor.guest_id, monitor.area_id ORDER BY monitor.date DESC LIMIT ".$page_print->page.",".$page_print->on_page);

	$form = $form.'<table width=100% border=1><tr><td><b>Guest</b></td><td><b>Unit</b></td><td><b>Steps</b></td><td><b>Right/Mistakes</b></td><td><b>Spent time</b></td><td><b>Start time</b></td><td><b>End time</b></td><td><b>Finish</b></td></tr>';
	for($i=0;$i<$result[count];$i++)
	 {
		$result_start = $mysql->Select("SELECT * FROM monitor WHERE guest_id='".$result[$i][guest_id]."' && area_id='".$result[$i][area_id]."' ORDER BY id ASC LIMIT 1");
		$result_end = $mysql->Select("SELECT * FROM monitor WHERE guest_id='".$result[$i][guest_id]."' && area_id='".$result[$i][area_id]."' ORDER BY id DESC LIMIT 1");

		$steps = $mysql->Select("SELECT count(*) as count FROM monitor WHERE status!=2 && guest_id='".$result[$i][guest_id]."' && area_id='".$result[$i][area_id]."'");
		$mistakes = $mysql->Select("SELECT count(*) as count FROM monitor WHERE status=0 && guest_id='".$result[$i][guest_id]."' && area_id='".$result[$i][area_id]."'");
		$rights = $mysql->Select("SELECT count(*) as count FROM monitor WHERE status=1 && guest_id='".$result[$i][guest_id]."' && area_id='".$result[$i][area_id]."'");
		$finish = $mysql->Select("SELECT * FROM monitor WHERE finish_code!='' && guest_id='".$result[$i][guest_id]."' && area_id='".$result[$i][area_id]."' LIMIT 1");


		$form = $form.'<tr><td><a href="?statistics&guest_id='.$result[$i][guest_id].'&id='.$result[$i][area_id].'">'.$result[$i][guest_id].'</a></td>
<td>'.$result[$i][area_text].'</td>
<td>'.$steps[0][count].'</td>
<td>'.$rights[0][count].'/'.$mistakes[0][count].'</td>
<td>'.round(($result_end[0][date]-$result_start[0][date])/60).' mins</td>
<td>'.date("m.d.y h:i a",$result_start[0][date]).'</td>
<td>'.date("m.d.y h:i a",$result_end[0][date]).'</td>
<td>'.($finish[0][finish_code]=="" ? "No" : $finish[0][finish_code]).'</td>
</tr>';
	 }
	$form = $form.'</table>';

if($page_print->max_page!=1 && $page_print->max_page!="")
 {
$form = $form.'<div class="list_pages">
'.$page_print->prev_page;

		$form = $form.$page_print->result;


$form = $form.$page_print->next_page.'</div>';
  }

	return $form;
 }

	/**
	* Show statistics for selected session
	* @return string data
	*/

 function statistics_id()
 {
	global $mysql;
	global $title;
	global $bread_menu;

	$result_id = $mysql->Select("SELECT monitor.*,ExpertiseArea.area_text FROM monitor
LEFT JOIN ExpertiseArea ON ExpertiseArea.area_id=monitor.area_id
WHERE monitor.guest_id='".$_GET[guest_id]."' && monitor.area_id='".$_GET[id]."'
 GROUP BY monitor.guest_id, monitor.area_id ORDER BY monitor.date DESC LIMIT 1");

	$bread_menu["?statistics"] = "Statistics";

	$title = 'Guest: '.$result_id[0][guest_id].' - '.$result_id[0][area_text];

$form = $form.'
<script type="text/javascript" src="js/info.js"></script>
';



		if($_GET[page]=="") $page = 1; else $page = $_GET[page];

		$page_print = new page_view('monitor','100',$page,' ','','<span>%i%</span>');
		$page_print->type = "control";
		$page_print->where = "monitor.guest_id='".$_GET[guest_id]."' && monitor.area_id='".$_GET[id]."'";

		$page_url = './main.php?statistics&guest_id='.$_GET[guest_id].'&id='.$_GET[id];

		$page_print->next_part = '<a href="'.$page_url.'&page=%i%">%name%</a>';
		$page_print->link = '<a href="'.$page_url.'&page=%i%">%i%</a>';

		$page_print->create();

		$page_print->prev_page('<a href="'.$page_url.'&page=%i%">Prev</a> ','<span>Prev</span> ');
		$page_print->next_page(' <a href="'.$page_url.'&page=%i%">Next</a>',' <span>Next</span>');

	$result = $mysql->Select("SELECT monitor.*,Questions.question_text,Answers.answer_text FROM monitor	
LEFT JOIN Questions ON Questions.id=monitor.question_id 
LEFT JOIN Answers ON Answers.id=monitor.answer_id 
WHERE monitor.guest_id='".$_GET[guest_id]."' && monitor.area_id='".$_GET[id]."' ORDER BY monitor.date DESC LIMIT ".$page_print->page.",".$page_print->on_page);


		$result_start = $mysql->Select("SELECT * FROM monitor WHERE guest_id='".$result[0][guest_id]."' && area_id='".$result[0][area_id]."' ORDER BY id ASC LIMIT 1");
		$result_end = $mysql->Select("SELECT * FROM monitor WHERE guest_id='".$result[0][guest_id]."' && area_id='".$result[0][area_id]."' ORDER BY id DESC LIMIT 1");

		$steps = $mysql->Select("SELECT count(*) as count FROM monitor WHERE status!=2 && guest_id='".$result[0][guest_id]."' && area_id='".$result[0][area_id]."'");
		$mistakes = $mysql->Select("SELECT count(*) as count FROM monitor WHERE status=0 && guest_id='".$result[0][guest_id]."' && area_id='".$result[0][area_id]."'");
		$rights = $mysql->Select("SELECT count(*) as count FROM monitor WHERE status=1 && guest_id='".$result[0][guest_id]."' && area_id='".$result[0][area_id]."'");
		$finish = $mysql->Select("SELECT * FROM monitor WHERE finish_code!='' && guest_id='".$result[0][guest_id]."' && area_id='".$result[0][area_id]."' LIMIT 1");



$form = $form.'
<b>Spent time:</b> '.round(($result_end[0][date]-$result_start[0][date])/60).' mins<br />
<b>Steps:</b> '.$steps[0][count].' <br />
<b>Right:</b> '.$rights[0][count].' <br />
<b>Wrong:</b> '.$mistakes[0][count].' <br />

<br />
';

	$form = $form.'
<table width=100% border=1><tr><td><b>Date</b></td><td><b>Spent time</b></td><td><b>Question</b></td><td><b>Answer</b></td><td><b>Status</b></td><td><b>IP</b></td></tr>';

	for($i=0;$i<$result[count];$i++)
	 {
		if($result[$i][status]=="2") $status = 'Start';
		else if($result[$i][status]=="0") $status = '<font color="red">False</font>';
		else if($result[$i][status]=="1") $status = '<font color="green">True</font>';
		else $status = '';

		if(strlen($result[$i][answer_text])>30) $answer_text = '<a href="#" class="popup_info">'.substr($result[$i][answer_text],0,30).'...</a><div style="display:none">'.$result[$i][answer_text].'</div>'; else $answer_text = $result[$i][answer_text];
		if($result[$i][status]!="2")
		$question_text = '<a href="interview_edit.php?area_id='.$result[$i][area_id].'&question_id='.$result[$i][question_id].'">Link</a> | <a href="#" class="popup_info">Text</a><div style="display:none">'.$result[$i][question_text].'</div>';
		else
		$question_text = '';

		if($result[$i+1][date]!="")
		 {
			$spent_time = $result[$i][date] - $result[$i+1][date];
			if($spent_time>60) $spent_time = round($spent_time/60).' mins';
			else  $spent_time = $spent_time.' sec';
		 }
		else
		 $spent_time = 0;

		$form = $form.'<tr>
<td>'.date("m.d.y h:i a",$result[$i][date]).'</td>
<td>'.$spent_time.'</td>
<td>'.$question_text.'</td>
<td>'.$answer_text.'</td>
<td>'.$status.'</td>
<td>'.$result[$i][ip].'</td>
</tr>';
	 }
	$form = $form.'</table>';

if($page_print->max_page!=1 && $page_print->max_page!="")
 {
$form = $form.'<div class="list_pages">
'.$page_print->prev_page;

		$form = $form.$page_print->result;


$form = $form.$page_print->next_page.'</div>';
  }

	return $form;
 }
?>
