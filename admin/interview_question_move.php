<?php 

include("../config_inc.php"); 

$insert = $_POST["question_id"];
$position = $_POST["position"] - 1;
$area_id = $_POST["area_id"];

function array_insert(&$array, $insert, $position) {
     $position = ($position == -1) ? (count($array)) : $position ;
     if($position != (count($array))) {
          $ta = $array;
          for($i = $position; $i < (count($array)); $i++) {
               if(!isset($array[$i])) {
                    die(print_r($array, 1)."\r\nInvalid array: All keys must be numerical and in sequence.");
               }
               $tmp[$i+1] = $array[$i];
               unset($ta[$i]);
          }
          $ta[$position] = $insert;
          $array = $ta + $tmp;
     } else {
          $array[$position] = $insert;          
     }
          
     ksort($array);
     return $array;
}


db_conn()
    or die ("Cannot connect to server");

$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
  or exit();

while ($myrow = mysql_fetch_array($result)) {
  $array[] = $myrow["id"];
  if ($myrow["id"] == $insert) {
    end($array);
    $key_to_be_deleted = key($array);
    #print "Should be at the question to move now: " . $myrow["id"] . "<br>\n";
    #print "Should be moved to the following key: " . $position . "<br>\n";
    #print "Here is the key to be deleted: " . $key_to_be_deleted . "<br><br>\n";
  }
  # printf("id: %s - %s - %s<br>\n",$myrow["id"],$myrow["question_id"],$myrow["question_text"]);
}

# print_r(array_values($array));

#print "<br><br>New with Inserted element:<br>\n";
# Move an element in the array
$array_out = array_insert($array,$insert,$position);
#print_r(array_values($array_out));

# Figure out if the key to be deleted has been moved or not...
if ($key_to_be_deleted > $position) {
  $key_to_be_deleted += 1;
}

#print "<br><br>New with deleted element: $key_to_be_deleted<br>\n";
# Now delete the element that was moved.
unset($array_out[$key_to_be_deleted]);
#print_r(array_values($array_out));

#print  "Now write correct order of question back to the database...<br>\n";
$counter = 1;
foreach($array_out as $value) {
  # Diagnostic to see if I'm getting the right fields.
  $result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id AND id = $value")
    or exit();
  $myrow = mysql_fetch_array($result);
  #print "area_id: " . $myrow["aera_id"] . "  question_id: " . $myrow["id"] . "  current_order: " . $myrow["question_id"] . "  New Order: " . $counter . "<br>\n";
  #$question_id_old = $myrow["question_id"];
  
  # Update the question_id field in the database with the new sort order, if it needs to be updated.
  if ($counter != $myrow["question_id"]) {
    $sql2 = "UPDATE Questions SET question_id=$counter WHERE area_id = $area_id AND id = $value";
    $result2 = mysql_query($sql2)
      or die(mysql_error());
    #print "Needs to be updated: $sql2<br>\n";
  }
  
  # Increment the counter one, so that the next loop will have the couter in the right palce for the reordering.
  $counter += 1;
}

print "The Question Sort Order Has Been Updated. <br><a href=\"interview_edit.php?area_id=$area_id&question_id=$insert\">Click here</a> to display updated sort order.";
# Return to the interview_edit.php page to the question that was just edited.
#
?>

