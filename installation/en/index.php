<?php
# if noncore/config_inc.php exists (and is not empty), go back to application index.php
if (file_exists( '../../noncore/config_inc.php' ) && filesize( '../../noncore/config_inc.php' ) > 15) {
	header( "Location: ../../index.php" );
	exit();
}

# include selected language
include_once("../../languages/lang-french.php");

# include versionning information
require_once( '../includes/version.php' );

# available languages
$langs = array (
    "en" => "English",
);

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Open Expert [<?php echo($_VERSION->_RELEASE) ?>] Web Installer</title>
	
	<meta http-equiv="content-style-type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../style/install.css" media="screen" />
	<link rel="shortcut icon" href="../style/img/expert.ico" type="image/x-icon" />
	
	<!-- prototype and scriptaculous -->
	<script src="../../js/prototype.js" type="text/javascript"></script>
	<script src="../../js/scriptaculous.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		function showContinue(){
			if ($('agreement').checked == true){
				$('continue').disabled = false;
			}
			else{
				$('continue').disabled = true;
			}
		}
		
		function changelang(lang){
		if (lang!='choose')
			document.location='../'+ lang +'/index.php'
		}
	</script>
	
</head>
<body>

<div id="global">
	<div id="entete">
		<h1>Welcome to OpenExpert Installation !!!</h1>
		<br />
		<p class="sous-titre">
			<img alt="" src="../style/img/openexpert-logo.gif" />
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><h3 style="padding-top: 10px">You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3><![endif]-->
			<comment><h3>You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3></comment>
			
		</p>
		<div style="clear: both; position: relative;">
			<h3><strong>Follow the steps bellow to complete installation :</strong></h3>
			<?/*?>
			<div style="position: absolute; right: 0px; bottom: 0px;">
				<select name="lang-select" onChange="changelang(this.options[this.selectedIndex].value); return false;">
					<option value="choose">Choose another language</option>
					<?php
					foreach ($langs as $lang => $langname) {
						echo "<option value='$lang'>$langname</option>";
					}
					?>
				</select>
			</div>
			<?*/?>
		</div>
	</div>

	<div id="centre">
		<div id="navigation">
			<div>
				<h2><u>Licence</u></h2>
			</div>
			<div>
				<h3>Permissions</h3>
			</div>
			<div>
				<h3>Database Settings</h3>
			</div>
			<div>
				<h3>Application Settings</h3>
			</div>
			<div>
				<h3>Branding</h3>
			</div>
			<div>
				<h3>Installation complete</h3>
			</div>
		</div>

		<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
		<!--[if IE]><div id="contenu" style="margin-top: -10px; padding-top: 0px;" ><![endif]-->
		<comment><div id="contenu"></comment>
			<h2 style="text-align: center;">This software is released under<br />the GNU General Public Licence.</h2>
			<h3>Please read the following text and agree to continue :</h3>
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><iframe src="../gnu-gpl.html" height="320px" width="510px" style="background:#f5f5dc; float:left;"></iframe><![endif]-->
			<comment><iframe src="../gnu-gpl.html" height="320px" width="510px" style="background:#f5f5dc;"></iframe></comment>
			<br />
			<p style="text-align: center;">
				<input type="checkbox" id="agreement" onClick="javascript:showContinue();">I agree</input>
			</p>
			<div id="nav-bottom">
				<form action="install2.php" style="text-align: center;">
					<div id="nav-bottom-right"><input type="submit" id="continue" value="Continue" disabled /></div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="pied">
		<p><?php echo($_VERSION->_URL) ?></p>
		<p><?php echo($_VERSION->_COPYRIGHT) ?></p>
	</div>
</div>

</body>
</html>
