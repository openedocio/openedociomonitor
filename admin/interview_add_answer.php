<?php
if($_POST['add_answer']=="Add a Choice"){
	
	/* Connection to Database */
	include("../config_inc.php"); 
	db_conn()
		or die ("Cannot connect to server");
	
	# Getting the area_id & question_id so we know where to insert an answer.
	$area_id = $_POST['area_id'];
	$question_id = $_POST['question_id'];
	
	# Figure out what the largest current ans_id is and the increment it one.
	$sql = "SELECT * FROM Answers WHERE question_id = $question_id AND area_id = $area_id ORDER BY ans_id DESC LIMIT 1";
	$result = mysql_query($sql);
	$myrow = mysql_fetch_array($result);
	$new_ans_id = $myrow["ans_id"] + 1;
	
	# Insert new answer to the appropriate question with the next ans_id which is in $new_ans_id.
	$sql = "INSERT INTO Answers (ans_id,question_id,area_id,next_question_id) VALUES ($new_ans_id,$question_id,$area_id,0)";
	$result = mysql_query($sql);
	
	# Call to subroutine to generate the current answers so that they can be outputted above the "new" question.
	interview_print_answers($area_id, $question_id); 

} else {
	0;
}
?>
