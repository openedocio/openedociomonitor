<?php 
include("../config_inc.php");
include("security_inc.php"); 

$section_type = "admin";

db_conn()
    or die ("Cannot connect to server");

include("$app_path/noncore/header.php");
include("navigation_inc.php");
include_once("fckeditor/fckeditor.php") ;

$area_id = $_GET["area_id"];
$question_id = $_GET["question_id"];
$sort_order = $_GET["sort_order"];
$hide_answers = $_COOKIE["hide_answers"];

# If the question_id variable is not set (usually because a person is starting the interview, then find what the first Question id is.
if ($question_id == NULL) {
        $result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id	ORDER BY question_id")
		or exit();
        $myrow = mysql_fetch_array($result);
        $question_id = $myrow["id"];
        $sort_order = $myrow["question_id"];
}

# Get the Interview name to Display at the top left of the page.
$result = mysql_query("SELECT area_text FROM ExpertiseArea WHERE area_id = $area_id")
  or exit();
$myrow = mysql_fetch_array($result);
$interview_name = $myrow["area_text"];

# Get the Interview question to be edited/displayed in the left column of the page.
$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id AND id = $question_id")
  or exit();
$myrow = mysql_fetch_array($result);
$question_text = stripslashes($myrow["question_text"]);
$help_text = stripslashes($myrow["help_text"]);
$sort_order = $myrow["question_id"];

?>

<link rel="stylesheet" href="../includes/css_text_box.js" type="text/js" />
<link rel="stylesheet" href="../includes/css_text_box.css" type="text/css" />

<script type="text/javascript">
function updateQuestion(){
  /*  The next two lines enable the updated FCKeditor text, to be submitted via the prototype ajax submit */
	jQuery("#FCKeditor1").val(CKEDITOR.instances.FCKeditor1.getData())
	jQuery("#FCKeditor2").val(CKEDITOR.instances.FCKeditor2.getData())
//  var oEditor = FCKeditorAPI.__Instances['FCKeditor1'] ;
//  oEditor.UpdateLinkedField() ;
  new Ajax.Updater( 'results', 'interview_save.php', { method: 'post', parameters: $('f_question').serialize() } );
}

function addAnswer(){
  /* This adds an additional answer to the current question */
  new Ajax.Updater( 'answers-area', 'interview_add_answer.php', { method: 'post', parameters: $('f_question').serialize() } );
}

function deleteAnswer(del_id,area_id,question_id){
  /*  This deletes an existing answer to the current question */
  var pars = 'del_id=' + del_id + '&area_id=' + area_id + '&question_id=' + question_id ;
  new Ajax.Updater( 'answers-area', 'interview_delete_answer.php', { method: 'post', parameters: pars } );
}

function deleteQuestion(area_id,del_question_id){
  /* Do a javascript popup to make sure that the user wants to delete the question */
  var answer = confirm("Delete question " + del_question_id + "?");
  if (answer) {
    theURL = 'interview_delete_question.php?del_question_id=' + del_question_id + '&area_id=' + area_id;
    location.href = theURL;
  } else {
    alert("No action taken")
  }
}

function edit_question(area_id,question_id){
  theURL = 'interview_edit.php?area_id=' + area_id + '&question_id=' + question_id + '#' + question_id;
  location.href = theURL;
}

function answers_hide_show(area_id,question_id,hide_answers){
  theURL = 'answers_hide_show.php?area_id=' + area_id + '&question_id=' + question_id + '&hide_answers=' + hide_answers ;
  location.href = theURL;
}

function addQuestion(area_id,question_id,answer_id){
  theURL = 'interview_add_question.php?area_id=' + area_id + '&question_id=' + question_id + '&answer_id=' + answer_id;
  location.href = theURL;
}

function updateMoveQuestion(area_id,id,postion){  
  /* Changes the order of the questions... when the drop down changes.  */
  new Ajax.Updater( 'results', 'interview_question_move.php', { method: 'post', parameters: $('f_question').serialize() } );
  
}  

</script>

<div id="edit-area">
<p><strong>Interview: <?php print(stripslashes($interview_name)); ?></strong>
(<a href="properties.php?area_id=<?php print $area_id ?>">edit properties</a>)</p>
<p><strong>Content # <?php print $sort_order ?> <div id="results">&nbsp;</div></strong></p>
<form  id="f_question"><p>
<input type="hidden" id="area_id" name="area_id" value="<?php print $area_id; ?>" />
<input type="hidden" id="question_id" name="question_id" value="<?php print $question_id; ?>" />
<input type="hidden" id="sort_order" name="sort_order" value="<?php print $sort_order; ?>" />

  <label>
    <?php
    # Creates rich text box.
    $oFCKeditor = new FCKeditor('FCKeditor1') ;
    $oFCKeditor->BasePath = 'fckeditor/' ;
    $oFCKeditor->ToolbarSet = 'Default';
    $oFCKeditor->Height = '250' ;
    $oFCKeditor->Value = $question_text;
    $oFCKeditor->Create() ;
    ?>  
  </label>
</p>
<script>
jQuery(document).ready(function(){
	jQuery('#help_status').click(function(){
	  if(jQuery(this).prop("checked")) {
	    jQuery(this).next().show();
	  } else {
	    jQuery(this).next().hide();
	  }
	});
});
</script>
<p><strong>Help</strong> <input type="checkbox" id="help_status" name="help_status" value="1"<? if($help_text!="") echo ' CHECKED';?>>
  <label<? if($help_text=="") echo ' style="display:none"';?>>
    <?php
    # Creates rich text box.
    $oFCKeditor = new FCKeditor('FCKeditor2') ;
    $oFCKeditor->BasePath = 'fckeditor/' ;
    $oFCKeditor->ToolbarSet = 'Default';
    $oFCKeditor->Height = '250' ;
    $oFCKeditor->Value = $help_text;
    $oFCKeditor->Create() ;
    ?>
</p>  
  </label>
<table border="0">
  <tr>
    <td width="245px"><strong>Choices</strong></td>
    <td width="40px"><strong>Link</strong></td>
    <td width="50px"><strong>Delete</strong></td>
  </tr>
</table>
<div id="answers-area">
    <?php
      # This builds the answer fields & delete buttons for each answer.
      # Inspiration for the Scriptulicous reorder code came from: http://zenofshen.com/posts/ajax-sortable-lists-tutorial
      interview_print_answers($area_id, $question_id);
    ?>
</div>

<script type="text/javascript" language="javascript" charset="utf-8">
  Sortable.create("answer_list", {
    onUpdate: function(area_id,question_id) {
      new Ajax.Request("interview_answer_order.php?area_id=<?php print $area_id ?>&question_id=<?php print $question_id ?>", {
        method: "post",
        parameters: { data: Sortable.serialize("answer_list") }
      });
    }
  });
</script>

<input type="button" name="add_answer" id="add_answer" value="Add a Choice" onclick="javascript:addAnswer();" /><br /><br />

<?php
  if ($myrow["carry_forward"] == 1) {
    $carry_forward = " checked=\"yes\"";
  } else {
    $carry_forward = "";
  }
?>
<p>Carry Forward Content to Summary at end of interview:
  <input type="checkbox" name="carry_forward" id="carry_forward" value="1" <?php print $carry_forward; ?>>
</p>
<p>Move this content before the following content:</p>
<p>
  <select name="position" id="position"  onchange="javascript:updateMoveQuestion()">
    <option value="0" selected="selected">Don't Move</option>
    <?php
      # Get a list of questions and populate a drop down box so that the user can reorder if he/she wants.

      $result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
	  or exit();
      while ($myrow = mysql_fetch_array($result)) {
        # Shorten question_text to a reasonable length.

	$question_text = mb_substr(strip_tags($myrow[question_text]), 0, 48, 'UTF-8');
	
        printf("<option value=\"%s\">%s - %s</option>",strip_tags($myrow["question_id"]),strip_tags($myrow["question_id"]),$question_text);
      }
    ?>
    </select>
</p>
<p>
  <input type="button" name="save" id="save" value="Save" onClick="javascript:updateQuestion();" />
  <input type="button" name="delete" id="delete" value="Delete"
         onClick="javascript:deleteQuestion(<?php print $area_id; ?>,<?php print $question_id ?>);" />
</p>
<p>&nbsp;</p>
</form>
</div>

<div id="scroll-area">
<div align="right">
  <?php
  if ($hide_answers == 1) {
    $label_text = "Hide";
    $hide_answers = 0;
  } else {
    $label_text = "Show";
    $hide_answers = 1;
  }
  
  print "<input type=\"button\" name=\"hide_answers\" id=\"hide_answers\"
      value=\"$label_text Choices\" onClick=\"javascript:answers_hide_show($area_id,
      $question_id,$hide_answers);\" />";
  ?>
  
  <input type="button" name="refresh_question_list" id="refresh_question_list"
    value="Refresh" onClick="javascript:window.location.reload(true);" />
  <input type="button" name="add_question" id="add_question" value="Add Another Content"
    onClick="javascript:addQuestion(<?php print $area_id; ?>,<?php print $question_id; ?>,0);" />
</div>
<table width="600" border="0" cellspacing="2">
  <tr>
    <td colspan="3" width="540px">&nbsp;</td>
    <td width="50px"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Carry Forward</strong></font></div></td>
    <td width="40px"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif"><strong>Next Link</strong></font></div></td>
    <td width="30px">&nbsp;</td>
  </tr>
</table>

<div id="tableContainer" class="tableContainer">
 
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="scrollTable"> 
 <tbody class="scrollContent">
<div id="questions-area">
<?php
# Call function to display the table with all the questions.
interview_print_questions($area_id,$question_id,$hide_answers);

?>
</div>
</tbody>
</table>
</div>
</div>
<p>&nbsp;</p>
</body>
</html>
