<?php
$section_type = "admin";
# This is an Export utility for OpenExpert...
include("../config_inc.php");
include("security_inc.php"); 

db_conn()
    or die ("Cannot connect to server");
    
	/**
	* Truncate null
	* @param string $fvalue text
	* @return string 
	*/
function replace_null ($fvalue)  {
    if ($fvalue == "") {
        $fvalue = "NULL";
    } else {
        $fvalue = "'$fvalue'";
    }
    return $fvalue;
}

$area_id = $_GET["area_id"];
$download = $_GET["download"];

# Get the interview Name information... this should be just one line from the database.
$sql = "SELECT * FROM ExpertiseArea WHERE area_id = $area_id";
$result = mysql_query($sql)
    or exit();
$myrow = mysql_fetch_array($result);
$area_id = $myrow["area_id"];
$area_text = $myrow["area_text"];
$author = replace_null($myrow["author"]);
$display_order = replace_null($myrow["display_order"]);
$visible = $myrow["visible"];
$description = replace_null($myrow["description"]);
$location = replace_null($myrow["location"]);
$version = replace_null($myrow["version"]);
$notes = replace_null($myrow["notes"]);
$language = replace_null($myrow["language"]);
$today = date("Y_m_d");

$title = str_replace(" ","_",$area_text);

# Write Header information for the file...
$sql_temp[] = "-- Begin OpenExpert Export\n";
array_push($sql_temp, "-- Interview: $area_text\n");
array_push($sql_temp, "-- Version: $version\n");
array_push($sql_temp, "-- Author: $author\n");
array_push($sql_temp, "-- Description: $description\n");
array_push($sql_temp, "-- Location: $location\n");
array_push($sql_temp, "-- Language: $language\n");
array_push($sql_temp, "-- Date: $today\n");
array_push($sql_temp, "-- Notes: $notes\n");

$area_id_temp = "-i001-";

# Write the Interview Information to ExpertiseArea table
array_push($sql_temp, "INSERT INTO `ExpertiseArea` VALUES ($area_id_temp, '$area_text', $author, $display_order, $visible, $description, $location, $version, $notes, $language);-- EOQ --\n");

# Write the Questions Information to the Questions table
$sql = "SELECT * FROM Questions WHERE area_id = $area_id";
$result = mysql_query($sql)
    or exit();
while ($myrow = mysql_fetch_array($result)) {
    $id = $myrow["id"];
    $id_temp_count += 1;
    $id_temp = "-q$id_temp_count-";
    $question_id = $myrow["question_id"];
    $area_id = $myrow["area_id"];
    $question_text = str_replace("'","''",$myrow["question_text"]);
    $carry_forward = $myrow["carry_forward"];
    $help = $myrow["help"];
    
    array_push($sql_temp, "INSERT INTO `Questions` VALUES ($id_temp, $question_id, $area_id_temp, '$question_text', $carry_forward, $help);-- EOQ --\n");
    
    $question_id_array[$id] = $id_temp;
    
    # Write the Answers Information to the Answers table
    $sql2 = "SELECT * FROM Answers WHERE question_id = $id";
    $result2 = mysql_query($sql2)
        or exit();
    while ($myrow2 = mysql_fetch_array($result2)) {
        $answer_id_temp_count += 1;
        $answer_id_temp = "-a$answer_id_temp_count-";
        $answer_id = $myrow2["id"];
        $ans_id = $myrow2["ans_id"];
        $question_id = $myrow2["question_id"];
        $next_question_id = $myrow2["next_question_id"];
        $answer_text = str_replace("'","''",$myrow2["answer_text"]);
        $area_id = $myrow2["area_id"];
        
        array_push($sql_temp, "INSERT INTO `Answers` VALUES ($answer_id_temp, $ans_id, $id_temp, $next_question_id, '$answer_text', $area_id_temp);-- EOQ --\n");
    }
}

# Loop through array and change the $next_question_id to the new place holder variable. and write
# the array element to the file.
$ourFileName = "oe_export_" . $title . "_" . $today . ".sql";
$ourFilePath = "$app_path/noncore/documents/$ourFileName";
$fh = fopen($ourFilePath, 'w') or die("can't open file");

foreach ($sql_temp as $sql) {
    $u = preg_match("#INSERT INTO `Answers` VALUES \(.*?,.*?,.*?, (.*?),.*?,.*?\)#msi",$sql,$array);
    if ($u) {
        $key = $array[1];
        # Look up what the $id_temp should be:
        $id_temp = $question_id_array[$key];
        
        # Do the replacement here.
        $sql = preg_replace("/, $key,/", ', ' . $id_temp . ',', $sql);
    }
    
    # Now write element to file....
    fwrite($fh, $sql);
}

# Close the writing to the file:
fclose($fh);


##############
# Do we want to display this on the page, or download?
##############
if ($download == 1) {
    # Start to download the file immediatly.
    header("Content-disposition: attachment; filename=$ourFileName");
    header('Content-type: application/octet-stream');
    readfile($ourFilePath);
    
    exit;
} else {
    # Display the header and footer along with a textarea showing the export file... a link to download the file
    # below the textarea.
    include("$app_path/noncore/header.php");
    include("navigation_inc.php");
    
    print "<p><form name=\"nofunction\">";
        print "<textarea name=\"sqldump\" cols=\"90\" rows=\"30\" wrap=\"OFF\">";
            $fh = fopen($ourFilePath, 'r') or die("can't open file");
            $theData = fread($fh, filesize($ourFilePath));
            fclose($fh);
            print $theData;
        print "</textarea>";
    print "</form></p>";
    
    printf("<p><a href='export.php?area_id=%s&download=1'>%s</a>....</p>",$area_id,_DOWNLOADFILE);
    
    include("$app_path/noncore/footer.php");
}


?>
