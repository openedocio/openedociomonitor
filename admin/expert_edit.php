<?php 
$section_type = "admin";
include("../config_inc.php"); 
include("security_inc.php");
db_conn()
    or die ("Cannot connect to server");

$area_id = $_GET["area_id"];
$add_question = $_POST["add_question"];
$add_answer = $_POST["add_answer"];
$Save = $_POST["Save"];

if ($add_question) {
	$new_question_id = $_POST["new_question_id"];
	$area_id = $_POST["area_id"];
	$new_question_text = $_POST["new_question_text"];
	$carry_forward = $_POST["carry_forward"];
	
	$sql = "INSERT INTO Questions (question_id,area_id,question_text,carry_forward) 
	VALUES ('$new_question_id','$area_id','$new_question_text','$carry_forward')";
  	$result = mysql_query($sql);

  	if (!$result) {
		print ("query failed\n");
		}
	else {
		header ("Location: expert_edit.php?area_id=$area_id"); 
    	exit;                 
	}
} elseif ($add_answer) {
	$new_ans_id = $_POST["new_ans_id"];
	$area_id = $_POST["area_id"];
	$question_id = $_POST["question_id"];
	$new_answer_text = $_POST["new_answer_text"];
	$next_question_id = $_POST["next_question_id"];
	
	$sql = "INSERT INTO Answers (ans_id,area_id,question_id,answer_text,next_question_id) 
		VALUES ('$new_ans_id','$area_id','$question_id','$new_answer_text','$next_question_id')";
  	$result = mysql_query($sql);

  	if (!$result) {
		print ("query failed\n");
		}
	else {
		header ("Location: expert_edit.php?area_id=$area_id"); 
    	exit;                 
	}
} elseif ($Save) {
	$area_text = $_POST["area_text"];
	$area_id = $_POST["area_id"];
	
	# Save Interview Title
		$sql = "UPDATE ExpertiseArea SET area_text='$area_text' WHERE area_id=$area_id";
		$update_sql = mysql_query($sql);
	
	# Save & Delete Questions
		# Get the questions from the DB and put them in an array.
		$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
			or exit();
		# loop through the Questions in the array for the area of expertise...
		while ($myrow = mysql_fetch_array($result)) {
			#### Check if any questions need deleting...####
			$d_temp = 'delete_question_' . $myrow[question_id];
			$delete_question = $_POST["$d_temp"];
			
			if ($delete_question >= 1) {
				#echo("Delete = $delete_question<br>");
				$sql = "DELETE FROM Questions WHERE id=$delete_question";
				$delete_sql = mysql_query($sql);
			}
			
			# update the question with the data in the feild
			$q_temp = 'question_text_' . $myrow[question_id];
			$question_text = $_POST["$q_temp"];
			$c_temp = 'carry_forward_' . $myrow[question_id];
			$carry_forward = $_POST["$c_temp"];
			$q_temp = 'question_id_' . $myrow[question_id];
			$question_id = $_POST["$q_temp"];
			$sql = "UPDATE Questions SET question_text='$question_text',carry_forward='$carry_forward' 
				WHERE area_id=$area_id AND question_id = '$question_id'";
			$update_sql = mysql_query($sql);
			
			# Get the Answers from the DB and put them in an array.
			$result2 = mysql_query("SELECT * FROM Answers WHERE area_id = $area_id AND question_id = $question_id ORDER BY ans_id")
				or exit();
			# loop through the Answers in the array for the area of expertise...
			while ($myrow2 = mysql_fetch_array($result2)) {
				# Save & Delete Answers (delete answers for questions deleted).
				$a_temp = 'answer_text_' . $myrow2[ans_id];
				#echo("AnswerTextVar = $a_temp |  ");
				$answer_text = $_POST["$a_temp"];
				#echo("AnswerText = $answer_text |  ");
				$n_temp = 'next_question_id_' . $myrow2[ans_id];
				$next_question_id = $_POST["$n_temp"];
				$question_id = $myrow2[question_id];
				$an_temp = 'answer_id_' . $myrow2[ans_id];
				#echo("AnswerID = $an_temp<br>");
				$answer_id = $_POST["$an_temp"];
				
				$sql = "UPDATE Answers SET answer_text='$answer_text', next_question_id='$next_question_id' 
				WHERE area_id=$area_id AND question_id = $question_id AND ans_id = $answer_id";
				$update_sql = mysql_query($sql);
				
				#Delete Answers
				$d_temp = 'delete_answer_' . $myrow2[ans_id];
				$delete_answer = $_POST["$d_temp"];
				if ($delete_answer >= 1) {
					#echo("Delete = $delete_answer<br>");
					$sql = "DELETE FROM Answers WHERE id=$delete_answer";
					$delete_sql = mysql_query($sql);
				}
			}
		}
} 

include("$app_path/noncore/header.php");
?>

<div align="left">
  <?php include("navigation_inc.php"); ?>
  </div>
<p>  
<?php   
	# This query gets the Area of Expertise Label Text 
    $result = mysql_query("SELECT * FROM ExpertiseArea WHERE area_id=$area_id")
        or exit();
    $myrow = mysql_fetch_array($result);
?>
</p>
<form method="post" action="expert_edit.php">
<input type="submit" name="Save" value="<?php echo(_SAVECHANGES) ?>">
	<input type="hidden" name="area_id" value="<?php echo($area_id) ?>">
<table width="780" border="1" cellspacing="2">
<tr><td colspan="3"><input name="area_text" type="text" size="80" value="<? echo($myrow["area_text"]) ?>"></td>
<td><font size="2" face="Arial, Helvetica, sans-serif"><?php echo(_CARRYFORWARD) ?></font></td>
<td><font size="2" face="Arial, Helvetica, sans-serif"><?php echo(_NEXTLINK) ?></font></td>
<td><font size="2" face="Arial, Helvetica, sans-serif"><?php echo(_DELETE) ?></font></td>
</tr>  
  <?php
  #This builds the Question fields
  $result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
        or exit();
		
	#Build string for drop down for $next_question_id in the answer section
	$result3 = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
		or exit();
	while ($myrow3 = mysql_fetch_array($result3)) {
		$drop_next_question_id .= "<option value=\"" . $myrow3["question_id"] . "\">" . $myrow3["question_id"] . "</option>\n";	
	}
	
 	while ($myrow = mysql_fetch_array($result)) {
		$question_id = $myrow["question_id"];
		
		if ($myrow["carry_forward"] == 1) {
			$carry_forward = " checked";
		} else {
			$carry_forward = "";
		}
		
		#Prints the Question Text
		printf("<tr><td valign=\"top\">%s.</td><td colspan=\"2\"><input type=\"hidden\" name=\"question_id_%s\" value=\"%s\">
			<input name=\"question_text_%s\" type=\"text\" size=\"75\" value=\"%s\">
			</td><td><input name=\"carry_forward_%s\" type=\"checkbox\" value=\"1\"%s></td><td>&nbsp;</td>
			<td><input type=\"checkbox\" name=\"delete_question_%s\" value=\"%s\">
			</td></tr>\n",$myrow["question_id"],$myrow["question_id"],$myrow["question_id"],$myrow["question_id"],
			$myrow["question_text"],$myrow["question_id"],$carry_forward,$myrow["question_id"],$myrow["id"]);
			
		# This builds the answer fields
		$result2 = mysql_query("SELECT * FROM Answers WHERE area_id = $area_id AND question_id = $question_id ORDER BY ans_id")
        	or exit();
		
		while ($myrow2 = mysql_fetch_array($result2)) {	
			# Track the biggest answer ID so that it can be used below to add additional answers.
			if ($biggest_answer < $myrow2["ans_id"]) {
				$biggest_answer = $myrow2["ans_id"];
			}
			# Output the answer fields...
			$ans_id = $myrow2["ans_id"];
			printf("<tr><td>&nbsp;</td><td>&nbsp;</td><td><input type=\"hidden\" name=\"answer_id_%s\" value=\"%s\">
				<input name=\"answer_text_%s\" type=\"text\" size=\"75\" value=\"%s\"></td>
				<td>&nbsp;</td><td valign=\"top\"><select name=\"next_question_id_%s\">
				<option value=\"%s\">%s</option><option value=\"---\">---</option>$drop_next_question_id</select></td>
				<td><input type=\"checkbox\" name=\"delete_answer_%s\" value=\"%s\">
				</td></tr>\n",$myrow2["ans_id"],$myrow2["ans_id"],$myrow2["ans_id"],$myrow2["answer_text"],
				$myrow2["ans_id"],$myrow2["next_question_id"],$myrow2["next_question_id"],$myrow2["ans_id"],
				$myrow2["id"]);
		}			
	} 
  ?>
</table>
<input type="submit" name="Save" value="<?php echo(_SAVECHANGES) ?>"><p>
</form>
<font size="2" face="Arial, Helvetica, sans-serif"><?php echo(_NEWQUESTION) ?>:</font><br>
<form method="post" action="expert_edit.php">
	<input type="hidden" name="area_id" value="<?php echo($area_id) ?>">
	<input type="hidden" name="new_question_id" value="<? echo($question_id + 1) ?>">
	<input name="new_question_text" type="text" size="80">
	<input type="submit" name="add_question" value="Add">
</form>

<font size="2" face="Arial, Helvetica, sans-serif"><?php echo(_NEWANSWER) ?>:</font><br>
<form method="post" action="expert_edit.php">
	<input type="hidden" name="area_id" value="<?php echo($area_id) ?>">
	<input type="hidden" name="new_ans_id" value="<?php echo($biggest_answer + 1) ?>">
	<input name="new_answer_text" type="text" size="80"> <?php echo(_QUESTION) ?>: 
	<select name="question_id">
		<?php
			$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
				or exit();
		  while ($myrow = mysql_fetch_array($result)) {
		  printf("<option value=\"%s\">%s</option>\n",$myrow["question_id"],$myrow["question_id"]);	
		  }
		?>
	</select>
	<?php echo(_LINKTO) ?>:
	<select name="next_question_id"><option value=""></option>
		<?php
			$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
				or exit();
		  while ($myrow = mysql_fetch_array($result)) {
		  printf("<option value=\"%s\">%s</option>\n",$myrow["question_id"],$myrow["question_id"]);	
		  }
		?>
	</select>
	<input type="submit" name="add_answer" value="<?php echo(_LINKTO) ?>">
</form>

<?php
include("$app_path/noncore/footer.php");
?>

