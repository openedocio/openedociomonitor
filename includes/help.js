// Global variables
var advHelpContent;

// functions
function basic_help(question_db_id){
Effect.SlideDown('help-area');

var url = 'includes/basic_help.php';
var pars = 'question_db_id=' + question_db_id;
var target = 'ajax-help-area';

var basicHelpAjax = new Ajax.Updater(target, url, {method: 'get', parameters: pars});

}

function advanced_help(question_db_id, question_id, area_id){
Effect.SlideDown('help-area');

var url = 'includes/advanced_help.php';
var pars = 'area_id=' + area_id + '&question_id=' + question_id + '&question_db_id=' + question_db_id;

new Ajax.Request(url, {
		method: 'get',
		parameters: pars,
		onSuccess: function(transport, json){
			advHelpContent = json;
			$('ajax-help-area').update(advHelpContent.question);
		}
	});

}


function mouseOver(answer_id){
// if there is a help text for this answer, display it in the 'help-area' div, if not then display default 'No help' text
if (!($('help-area').style.display == 'none'))
	$('ajax-help-area').update(advHelpContent.answers[answer_id]);

}


function mouseOut(answer_id){
// if there is a custom help text for this question, display it in the 'help-area' div, if not then display default 'help' text for a question
if (!($('help-area').style.display == 'none'))
	$('ajax-help-area').update(advHelpContent.question);

}