<?php 

##############################################################################
# This is the installation file generated from the install wizard 
# If something as gone wrong and you need to set parameters manually, 
# you should better edit branding_inc-default.php (which as a valid syntax) 
# and rename it to branding_inc.php. 
##############################################################################

# This is put in title bar of web browser
$application_name = 'OpenExpert.org';

# Icon to display in browser
$shortcut = "$nav_path/noncore/images/openexpert.ico";

# Logo to display
$logo_lg = "$nav_path/noncore/images/openexpert-logo.gif";

# Header text
$top_text = "";

# Help text
$settings[help_text] = "false";

?>