<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","ISO-8859-1");
define("_HOME","Accueil");
define("_FAQ","FAQ");
define("_ABOUT","A propos");
define("_ADMIN","Admin");
define("_AREASEXPERTISE","Interviews");
define("_SPONSORS","Sponsors");
define("_FOLLOWUP","Objets ‡ suivre");
define("_FAQLONG","Foire Aux Questions");
define("_QUESTION","Interview");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">Page prÈcÈdente</a> ]");
define("_FAQ_TEXT","<h1>Doing an Edocio</h1><p>Read each page or screen as presented to you. Then select from the 5 sentences presented. Select the one contained in the page or screen you just read. <br /><br /><b>Take care to select the correct sentence because choosing a sentence not contained in the page you just read will send you one or more back not forward. </b></p>");
define("_HEADER_TITLE","<h2>The Role of Philosophy<br>in the Evolution of Western Culture</h2><h3>By Wallace H Provost Jr.</h3>");

define("_INPUT_FINISH_CODE","Input a finish code from a previous unit");
define("_YOUR_CODE_WRONG","Your code is wrong!");

define("_PLAY_AUDIO","Play the Audio");
define("_PAUSE_AUDIO","Pause the Audio");
define("_INCREASE_VOLUME","Increase Volume");
define("_DECREASE_VOLUME","Decrease Volume");

# Admin Text
define("_NEWEXPERTISE","Ajouter une Nouvelle Interview");
define("_ADMINHOME","Tableau de bord de l'Administrateur");
define("_USERS","Utilisateurs");
define("_SETTINGS","Options");
define("_ADD","Ajouter");
define("_SAVECHANGES","Enregistrer les changements");
define("_CARRYFORWARD","Faire suivre");
define("_NEXTLINK","Liens Suivants");
define("_DELETE","Supprimer");
define("_NEWQUESTION","Nouvelle Question");
define("_NEWANSWER","Nouvelle RÈponse");
define("_LINKTO","Lien vers");
define("_CONFIRMDELETE", "Etes-vous s˚r de vouloir supprimer cette interview?");
define("_ADMINLOGIN", "Connexion de l'Administrateur");
define("_USERNAME", "Nom d'Utilisateur");
define("_PASSWORD", "Mot de Passe");
define("_PASSWORDERROR", "Le mot de passe ne correspond pas, rÈessayez s'il vous plait");

# Help Text
define("_HIDE_HELP","Masquer l'aide");

# Need to Translate:
# Admin Text
define("_PROPERTIES","Properties");
define("_HIDE","Hide");
define("_SHOW","Show");
define("_REALLYDELETE","Really Delete?");
define("_EXPORT","Export");
define("_IMPORT","Import Interviews");
define("_DOWNLOADFILE","Download this as an export file");
# Help Text
define("_NO_HELP_TEXT","No help available");
define("_ADVANCED_HELP_TEXT","Click to get help");
define("_NEED_HELP","Need Help ?");
define("_MORE_TOOLS","More tools");
define("_NO_HELP_AVAILABLE","No help available for this item");
define("_DEFAULT_ADV_HELP_TEXT","Hover an answer to get help about it");
define("_DEFAULT_ADV_NO_HELP_TEXT","No help is available for this answer");

?>