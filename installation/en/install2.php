<?php
# include selected language
include_once("../../languages/lang-french.php");

# include versionning information
require_once( '../includes/version.php' );

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Open Expert [<?php echo($_VERSION->_RELEASE) ?>] Web Installer</title>
	
	<meta http-equiv="content-style-type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../style/install.css" media="screen" />
	<link rel="shortcut icon" href="../style/img/expert.ico" type="image/x-icon" />
	
	<!-- prototype and scriptaculous -->
	<script src="../../js/prototype.js" type="text/javascript"></script>
	<script src="../../js/scriptaculous.js" type="text/javascript"></script>
	
</head>
<body>

<div id="global">
	<div id="entete">
		<h1>Welcome to Open Expert Installation !!!</h1>
		<br />
		<p class="sous-titre">
			<img alt="" src="../style/img/openexpert-logo.gif" />
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><h3 style="padding-top: 10px">You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3><![endif]-->
			<comment><h3>You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3></comment>
			
		</p>
		<p style="clear: both;">
			<h3><strong>Follow the steps bellow to complete installation :</strong></h3>
		</p>
	</div>

	<div id="centre">
		<div id="navigation">
			<div>
				<h3>Licence</h3>
			</div>
			<div>
				<h2><u>Permissions</u></h2>
			</div>
			<div>
				<h3>Database Settings</h3>
			</div>
			<div>
				<h3>Application Settings</h3>
			</div>
			<div>
				<h3>Branding</h3>
			</div>
			<div>
				<h3>Installation complete</h3>
			</div>
		</div>

		<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
		<!--[if IE]><div id="contenu" style="margin-top: -10px; padding-top: 0px;" ><![endif]-->
		<comment><div id="contenu"></comment>
			<h2 style="text-align: center;">Checking correct files and directories permissions</h2>
			<h3>If some of the parameters below appear in red, please fix them in order to continue installation.</h3>
			
			<!-- Parameters check -->
			<?php $errors = 0; ?>
			<table>
				<tr>
					<td valign="top">
					<strong>*</strong> 
					</td>
					<td valign="top">
					MySQL Support
					</td>
					<td valign="top">
					=&gt;
					</td>
					<td valign="top" >
					<?php
					echo function_exists( 'mysql_connect' ) ? '<b><font color="green">Yes</font></b>' : '<b><font color="red">No</font></b>';
					if (!function_exists( 'mysql_connect' ))
						$errors = 101;
					?>
					</td>
				</tr>
				<tr>
					<td valign="top">
					<strong>*</strong> 
					</td>
					<td valign="top">
					User configuration file
					</td>
					<td valign="top">
					=&gt;
					</td>
					<td valign="top">
					<?php
					if (@file_exists('../../noncore/config_inc.php') &&  @is_writable( '../../noncore/config_inc.php' )){
						echo '<b><font color="green">noncore/config_inc.php is editable</font></b>';
					} else if (@file_exists('../../noncore/config_inc.php') && !@is_writable( '../../noncore/config_inc.php' )) {
						$errors++;
						echo '<b><font color="red">noncore/config_inc.php is not editable</font></b>';
					} else if (@is_writable( '../../noncore' )) {
						echo '<b><font color="green">noncore/config_inc.php is not present but noncore directory is editable</font></b>';
					} else {
						$errors++;
						echo '<b><font color="red">noncore/config_inc.php is not editable (or not present) and noncore directory is not editable</font></b>';
					} ?>
					</td>
				</tr>
				<tr>
					<td valign="top">
					<strong>*</strong> 
					</td>
					<td valign="top">
					User branding file
					</td>
					<td valign="top">
					=&gt;
					</td>
					<td valign="top" >
					<?php
					if (@file_exists('../../noncore/branding_inc.php') &&  @is_writable( '../../noncore/branding_inc.php' )){
						echo '<b><font color="green">noncore/branding_inc.php is editable</font></b>';
					} else if (@file_exists('../../noncore/branding_inc.php') && !@is_writable( '../../noncore/branding_inc.php' )) {
						$errors++;
						echo '<b><font color="red">noncore/branding_inc.php is not editable</font></b>';
					} else if (@is_writable( '../../noncore' )) {
						echo '<b><font color="green">noncore/branding_inc.php is not present but noncore directory is editable</font></b>';
					} else {
						$errors++;
						echo '<b><font color="red">noncore/branding_inc.php is not editable (or not present) and noncore directory is not editable</font></b>';
					} ?>
					</td>
				</tr>
				<tr>
					<td valign="top">
					<strong>*</strong> 
					</td>
					<td valign="top">
					Documents directory for user
					</td>
					<td valign="top">
					=&gt;
					</td>
					<td valign="top" >
					<?php
					if (@is_writable( '../../noncore/documents' )){
						echo '<b><font color="green">noncore/documents/ is editable</font></b>';
					} else {
						$errors++;
						echo '<b><font color="red">noncore/documents/ is not editable</font></b>';
					} ?>
					</td>
				</tr>
				<tr>
					<td valign="top">
					<strong>*</strong> 
					</td>
					<td valign="top">
					Image directory for user
					</td>
					<td valign="top">
					=&gt;
					</td>
					<td valign="top" >
					<?php
					if (@is_writable( '../../noncore/images' )){
						echo '<b><font color="green">noncore/images/ is editable</font></b>';
					} else {
						$errors++;
						echo '<b><font color="red">noncore/images/ is not editable</font></b>';
					} ?>
					</td>
				</tr>
			</table>
			<br />
			<p>
			<?php if($errors > 0){
				if ($errors > 100) {
				echo '<b><font color="red">Critical error : MySQL is not supported. Installation cannot continue. Please contact your server provider to fix this problem.</font></b>';
				} else {
				echo '<b><font color="red">At least one of the above files or directories is not editable</font></b><br /><p>Click on the "Refresh" button to check parameters again and unlock "Continue" button.</p>';
				}
			}
			?>
			</p>
			<br />
			<p style="text-align: center;">
				<?php if($errors > 0 && $errors < 100){
					echo '<input type="button" onClick="javascript:location.reload(true)" value="Refresh" />';
				} ?>		
			</p>
			<div id="nav-bottom">
				<form action="install3.php" style="text-align: center;">
					<div id="nav-bottom-left"><a href="index.php">&lt; Back to licence</a></div>
					<div id="nav-bottom-right"><input type="submit" id="continue" value="Continue" disabled /></div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="pied">
		<p><?php echo($_VERSION->_URL) ?></p>
		<p><?php echo($_VERSION->_COPYRIGHT) ?></p>
	</div>
</div>

<script type="text/javascript">
	var err_count = <?php echo($errors); ?>;
	if (err_count == 0){
		$('continue').disabled = false;
	}
	else{
		$('continue').disabled = true;
	}
</script>

</body>
</html>
