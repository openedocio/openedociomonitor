<?php 
# checks for configuration file, if none found loads installation page
if (!file_exists( 'noncore/config_inc.php' ) || filesize( 'noncore/config_inc.php' ) < 15) {
	$self = rtrim( dirname( $_SERVER['PHP_SELF'] ), '/\\' ) . '/';
	header("Location: http://" . $_SERVER['HTTP_HOST'] . $self . "installation/index.php" );
	exit();
}
# end of installation check


# We're not in the installation procedure, then launch the application
include("config_inc.php"); 

db_conn()
    or die ("Cannot connect to server");

include("$app_path/noncore/header.php");
?>
  <h3 align="left"><font class="title"><? echo(_AREASEXPERTISE) ?></font></h3>
  
  <p>
    <ul>
		<?php
			# Display visible interviews
			$result = mysql_query("SELECT * FROM ExpertiseArea WHERE visible = 1 ORDER BY display_order");
                        if (!$result) {
                            die('Invalid query: ' . mysql_error());
                        }
			
			while ($myrow = mysql_fetch_array($result)) {   
				printf("<li><a href=\"expert_wizard.php?area_id=%s\">%s</a></li>",$myrow["area_id"],stripslashes($myrow["area_text"]));		
			}
		?>
	</ul>
  </p>
<?php 
   
include("$app_path/noncore/footer.php");
?>