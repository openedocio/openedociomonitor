<?php
# Direct Access is not allowed
if (eregi("version.php",$_SERVER['PHP_SELF'])) {
    Header("Location: ../index.php");
    die();
}

# Product and version informations
class OpenExpertVersion {
	# Product name
	var $_PRODUCT = "OpenExpert";
	# Release
	var $_RELEASE = "0.5.12";
	# Dev status
	var $_DEV_STATUS = "development";
	# Copyright
	var $_COPYRIGHT = "Copyright (C) 2008 - 2014 OpenExpert";	
	# Copyright URL
	var $_URL = '<a href="#">OpenExpert</a> is a free software released under GNU/GPL licence.';

}

# generate version object
$_VERSION = new OpenExpertVersion();

?>
