<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","UTF-8");
define("_HOME","Начало");
define("_FAQ","Инструкции");
define("_ABOUT","О нас");
define("_ADMIN","Вход инструктора");
define("_AREASEXPERTISE","Части");
define("_SPONSORS","Спонсоры");
define("_FOLLOWUP","Подробно");
define("_FAQLONG","Часто задаваемые вопросы");
define("_QUESTION","Вопрос");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">Назад</a> ]");
define("_YOU_APPORXIMATELY","Вы закончили");
define("_YOU_APPORXIMATELY_COMPLETE","примерно");
define("_START_OVER","Начать сначала");
define("_ITEMS_TO_FOLLOW_UP_ON","Посмотреть более подробно");
define("_EMAIL_OR_PRINT","Отправить на Email или распечатать");
define("_YOUR_STAMP_IS","Ваш код");

define("_INPUT_FINISH_CODE","Введите полученный код из предыдущей части");
define("_YOUR_CODE_WRONG","Ваш код неверный");

define("_FAQ_TEXT","<h1>Руководство OpenExpert</h1><p>Прочитайте каждую страницу или изображение показанное вам. Выберите одно из 5 предложенных вам предложений, по информации которую вы уже прочли.<br /><br /><b>Будьте внимательны при выборе правильного предложения, потому что если вы сделаете неправильный выбор, вы будете перемещены на несколько шагов назад.</b></p>");

define("_PLAY_AUDIO","Прослушать аудио");
define("_PAUSE_AUDIO","Остановить аудио");
define("_INCREASE_VOLUME","Увеличить звук");
define("_DECREASE_VOLUME","Понизить звук");

# Admin Text
define("_NEWEXPERTISE","Создать новую главу");
define("_ADMINHOME","Администрирование домой");
define("_USERS","Пользователи");
define("_SETTINGS","Настройки");
define("_ADD","Добавить");
define("_SAVECHANGES","Сохранить изменения");
define("_CARRYFORWARD","Переход к следующей странице");
define("_NEXTLINK","Следующая ссылка");
define("_DELETE","Удалить");
define("_NEWQUESTION","Новый вопрос");
define("_NEWANSWER","Новый ответ");
define("_LINKTO","Ссылка на");
define("_CONFIRMDELETE", "Вы на самом деле хотите удалить учебник?");
define("_ADMINLOGIN", "Админ - вход");
define("_USERNAME", "Имя пользователя");
define("_PASSWORD", "Пароль");
define("_PASSWORDERROR", "Имя пользователя или пароль не совпадают");
define("_PROPERTIES","Свойства");
define("_HIDE","Спрятать");
define("_SHOW","Показать");
define("_REALLYDELETE","Подтвердить удаление?");
define("_EXPORT","Экспорт");
define("_IMPORT","Импорт учебников");
define("_DOWNLOADFILE","Скачать как экспортированный файл");


# Help Text
define("_NO_HELP_TEXT","Помощь недоступна");
define("_ADVANCED_HELP_TEXT","Нажмите для получения подсказки");
define("_NEED_HELP","Необходима помощь?");
define("_MORE_TOOLS","Дополнительные инструменты");
define("_HIDE_HELP","Спрятать помощь");
define("_NO_HELP_AVAILABLE","Помощь недоступна для этого элемента");
define("_DEFAULT_ADV_HELP_TEXT","Наведите на вопрос для получения помощи");
define("_DEFAULT_ADV_NO_HELP_TEXT","Помощь недоступна для этого ответа");

?>