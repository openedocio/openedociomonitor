<?php

# Get data
$appPath = $_GET["appPath"];
$navPath = $_GET["navPath"];
$adminName = $_GET["adminName"];
$adminPwd = $_GET["adminPwd"];

# form text to write
$text = 	"# Administrator user name and password \n" .
		"\$user = '$adminName'; \n" .
		"\$password = '$adminPwd'; \n\n" .
		"# Path to the root of your website on your web server. \n" .
		"\$app_path = '$appPath'; \n\n" .
		"# URL of the site \n" .
		"\$nav_path = '$navPath'; \n\n" .
		"# Site width\n" .
		"\$settings[site_width] = '100%';\n\n" .
		"# You need input a finish code from a previous unit to start\n" .
		"\$settings[unit_code] = true;\n\n" .
		"# Default language\n" .
		"\$settings[default_lang] = 'en';\n\n" .
		"?>";

# result to pass to callback function
$result = '';

# open noncore/config_inc.php (append text to existing one)
$f = "../../noncore/config_inc.php";
$handle = fopen($f, "a+");

# write data to config_inc.php
if (is_writable($f)) {
    if (fwrite($handle, $text) === FALSE) {
		$result = 'An error occured while writting data in ' . $fp;
    } else {   
	    $result = 1;   
		fclose($handle);
		# chmod it to 777
		@chmod ($f, 0777);
	}               
}
else {
    $result = $f . 'is not writeable';
} 

# return the result	
echo $result;

?>