<?php 
$section_type = "admin";
include("../config_inc.php");
include("security_inc.php");

db_conn()
    or die ("Cannot connect to server");

$submit = $_POST["submit"];
$confirm = $_POST["confirm"];
$file_name = $_POST["file_name"];

$uploaddir = "$app_path/noncore/documents/";

include("$app_path/noncore/header.php");
include("navigation_inc.php");

?>

<h2>Import an Interview</h2>

<?php

if ($submit) {
	# set new pictures name to the date and time
	$uploadfile = $uploaddir . "oe_import_" . date("Y_m_d_G_i_s") . ".sql";
	
	if (move_uploaded_file($_FILES['import_file']['tmp_name'], $uploadfile)) {
		print "<p>Step 1: Wonderful, the file has been successfully uploaded to the server...</p>\n";
		
		# Now test to see if the file contains valid SQL that would
		# Import without error into the database.
		print "<p>Step 2: Please look at the SQL in the box below to see if anything looks
			suspicious before completing the final import step.</p>\n";
		
		$fh = fopen($uploadfile, 'r') or die("can't open file");
		print "<textarea cols='100' rows='20'>";
		while (!feof($fh)) {
			$buffer = fgets($fh);
			print $buffer;
		}
		print "</textarea>";
		fclose($fh);
		
		# Put the button here to either import the interview, or abort.
		print "<form method=\"post\" action=\"import.php\">";
			print "<input type=\"submit\" name=\"confirm\" value=\"Confirm Upload\">";
			print "<input type=\"hidden\" name=\"file_name\" value =\"$uploadfile\">";
		print "</form>";
		
	} else {
		print "<p>Possible file upload attack!  Please contact the webmaster
		if you feel that you've received this message in error. <a href=\"index.php\">Click here</a> to return to
		the main admin page.</p>\n";
		exit;
	}
        
	# Feedback that file was successfully imported and a link back to the
	# main admin page...
  	print "<p>You can return to the <a href=\"index.php\">main admin page</a> when you're ready...</p>";
	
     	exit;
	
} elseif ($confirm) {
	# Run the SQL in the file here... checking for overlapping key field id's
	# (ExpertiseArea.area_id & Questions.id and fixing where found.
	$fh = fopen($file_name, 'r') or die("can't open file $file_name");
	$temp_file = $uploaddir . "temp_file.sql";
	$fh2 = fopen($temp_file, 'w') or die ("can't open file temp_file.sql");
	print "$temp_file\n\n";
	print "<textarea cols='100' rows='20'>";

	while (!feof($fh)) {
		$buffer = fgets($fh);
		# Take out the -- EOQ -- or "End of Query" marker that causes queries to fail
		$sql = str_replace("-- EOQ --","",$buffer);
		
		# Check to see if we're on an ExpertiseArea line... then find the last one in the db and incement by 1.
		$i = preg_match("#ExpertiseArea` VALUES \((.*?),#msi",$sql,$array);  
		if ($i){
			$area_id_orig = $array[1];
			# Checks to see if we have an area_id... if we do, increment it by one, if not, find the next
			# available area_id number and use it instead.
			if ($area_id > 0) {
				$area_id += 1;
			} else {
				$sql3 = "SELECT * FROM ExpertiseArea ORDER BY area_id DESC LIMIT 1";
				$result3 = mysql_query($sql3)
					or exit();
				$myrow3 = mysql_fetch_array($result3);
				$area_id = $myrow3["area_id"] + 1;
			}
			# Now replace the origional $area_id.
			$sql = preg_replace("/\($area_id_orig,/", '(' . $area_id . ',', $sql);
			#print "Interview sql: $sql\n";
		}
		
		# Check Questions Table and change area_id there, as well as check to see if "id" is unique.  If not,
		# then Answers.question_id needs to be changed as well.
		$e = preg_match("#INSERT INTO `Questions` VALUES \((.*?),.*?,(.*?),#msi",$sql,$array);
		
		if ($e) {
			$question_id_orig = $array[1];
			$area_id_orig = $array[2];
			
			# Checks to see if we have an question_id... if we do, increment it by one, if not, find the next
			# available question_id number and use it instead.
			if ($question_id > 0) {
				$question_id += 1;
			} else {
				$sql4 = "SELECT * FROM Questions ORDER BY id DESC LIMIT 1";
				$result4 = mysql_query($sql4)
				    or exit();
				$myrow4 = mysql_fetch_array($result4);
				$question_id = $myrow4["id"] + 1;
			}
			
			# Put the $question_id and $question_id_old in an array so that they can be looked up at the end.
			# to populate the $next_question_id field... we can't do this now as we may be asked to replace a
			# place holder that we haven't seen yet.  Will do a search and replace on the file that is being created.
			$question_id_array[$question_id] = $question_id_orig;
			
			# Replace the origional $question_id
			$sql = preg_replace("/\($question_id_orig,/", '(' . $question_id . ',', $sql);
			
			# Now replace the origional $area_id...
			$sql = preg_replace("/,$area_id_orig,/", ', ' . $area_id . ',', $sql);
			#print "Question sql: $sql\n";
			#print_r($question_id_array);
		}
		
		# Check Answers Table and change area_id there, as well as the question_id. See if the Answers.id is unique.
		# If not, then Answers.id needs to be changed as well.
		$u = preg_match("#INSERT INTO `Answers` VALUES \((.*?),.*?,(.*?),.*?,.*?,(.*?)\)#msi",$sql,$array);
		
		if ($u) {
			$answer_id_orig = $array[1];
			$question_id_orig = $array[2];
			$area_id_orig = $array[3];
			
			# Checks to see if we have an answer_id... if we do, increment it by one, if not, find the next
			# available answer_id number and use it instead.
			if ($answer_id > 0) {
				$answer_id += 1;
			} else {
				$sql5 = "SELECT * FROM Answers ORDER BY id DESC LIMIT 1";
				$result5 = mysql_query($sql5)
				    or exit();
				$myrow5 = mysql_fetch_array($result5);
				$answer_id = $myrow5["id"] + 1;
			}
			
			# Now replace the origional $area_id...
			$sql = preg_replace("/,$area_id_orig\)/", ', ' . $area_id . ')', $sql) ;
			
			# Replace the origional $question_id
			$sql = preg_replace("/,$question_id_orig,/", ', ' . $question_id . ',', $sql);
			
			# Replace the origional $answer_id
			$sql = preg_replace("/\($answer_id_orig,/", '(' . $answer_id . ',', $sql);
			#print "Question sql: $sql\n";
			#print_r($question_id_array);
		}
		
		
		# If there is no text on the line (i.e. a blank line), then skip the line so no error messages appear...
		# I was executing this directly, but will now put in a big variable so that I can search and replace the
		# $next_question_id field.
		if (strlen($sql) > 4) {
			#print "$sql";
			fwrite($fh2, "$sql");
		}
	}
	
	fclose($fh2);
	fclose($fh);
	
	# Now Open the temp file that was created with the sql script, and loop through the $question_id_array
	# created  when looking at the "Questions" queries and replace all the place holders for the and replace
	# all the $next_question_id place holders with the proper $question_id's.
	$fh2 = fopen($temp_file, 'r') or die ("can't open file temp_file.sql");
	$theData = fread($fh2, filesize($temp_file));
	#echo $theData;
	fclose($fh2);
	
	# Loop through $question_id_array and replace all the placehholders in $theData... then write to database after.
	while (list ($key, $val) = each ($question_id_array)) {
		#echo "'$key' -> '$val'<br>\n";
		$theData = str_replace($val,$key,$theData);
	}
	
	$sql_array = explode("\n",$theData);
	foreach ($sql_array as $sql) {
		mysql_query($sql);
		# Print out any error message that may appear.
		#print "$sql\n";
		if (strlen($sql) > 4) {
			if (mysql_errno()) {
				print "MySQL error ".mysql_errno().": ".mysql_error()." - When executing:\n";
			}	
		}	
	}
	print "</textarea>";
	
	print "<p>Any errors encountered in uploading the interview will appear in the box above.  If there are no
		error messagages, then you should be ready to modify or use the interview immediatly.  Remember that
		the interview is now available on your website for people to take.  If you don't want people to use
		the intverview right away, go to the <a href=\"index.php\">main admin page</a> and 'Hide' the interview.</p>";
} else {
	# the html for the default page goes here
	?>
		<p>OpenExpert interview files are files with SQL formatted code that can be
		imported into the local OpenExpert database.  You should only import interviews from
		sources that you trust (like <a href="http://openexpert.org">OpenExpert.org</a>).
		If you have any interviews you'd like to share, just
		export the interview, and then upload it to the OpenExpert.org website.</p>
		
		<form enctype="multipart/form-data" method="post" action="import.php">
		<table align="center" border="0" cellPadding="1" cellSpacing="1" id="TABLE1" width="400">
		  
		    <tr>
			<td>
			    <p align="right">OpenExpert File</p></td>
			<td>
			    <input type="File" name="import_file" size="25">
			</td>
		    </tr>
		    <tr>
			<td colSpan="2" width="100%" align="center">
			    <input type="submit" name="submit" value="Upload">
			</td>
		    </tr>
		</table>
		</form>
	
	<?php

}

include("$app_path/noncore/footer.php");
?>

