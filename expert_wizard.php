<?php 
include("config_inc.php"); 
db_conn()
    or die ("Cannot connect to server");

session_start();

if(isset($_POST[code]) && isset($_POST["question_id"]))
 {
	$question_id = intval(crypt_key($_POST["question_id"],crypt_key(substr($_POST[code],12),substr($_POST[code],0,12),1),1));
	$answer_id = intval(crypt_key($_POST["answer_id"],crypt_key(substr($_POST[code],12),substr($_POST[code],0,12),1),1));

	if(!isset($_POST[content_id])) $question_id = null;
	else $content_id = intval(crypt_key($_POST["content_id"],crypt_key(substr($_POST[code],12),substr($_POST[code],0,12),1),1));
 }

$area_id = $_GET["area_id"];
$num_rows = $_COOKIE["num_rows"];

$permission_text = permission_unit($area_id);


# See if there is anything in the $carry_forward_text for this session_id in the carry_forward_temp table.
$result = mysql_query("SELECT * FROM carry_forward_temp WHERE session_id = '$session_id' ORDER BY id")
        or die(mysql_error());
$num_rows_cf = mysql_num_rows($result);

#print "num_rows: $num_rows<br>\n";
#print "session_id: $session_id<br>\n";

if ($num_rows_cf >= 1) {
        $carry_forward_text = "1";
} else {
        $carry_forward_text = "";
}

# If a question id was not passed to the page, the find out what the first question id is, and start there.
if ($question_id == NULL) {
        $result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id	ORDER BY question_id")
		or die(mysql_error());
        $myrow = mysql_fetch_array($result);
        $question_id = $myrow["id"];
        
        # Reset carry forward text variable and cookie.
        $result = mysql_query("DELETE FROM carry_forward_temp WHERE session_id = '$session_id'")
                or die(mysql_error());
        
        # Figure out how many questions are in the interview so we can let the user know where they are in the interview.
	$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id")
                or die(mysql_error());
	$num_rows = mysql_num_rows($result);  # This tells os how many questions there are in total for this interview.
	setcookie('num_rows',$num_rows);


	 monitor_insert_result($area_id,0,0,$answer_id,$myrow["question_id"]);

}
else
{
        $result_question_page = mysql_query("SELECT id,question_id FROM Questions WHERE area_id = '".$area_id."' && id='".$content_id."' ORDER BY question_id LIMIT 1")
		or die(mysql_error());
        $result_question_page = mysql_fetch_array($result_question_page);

       $result_answers_count = mysql_query("SELECT count(*) as count FROM Answers WHERE area_id = ".$area_id." AND question_id = '".$result_question_page[id]."'")
		or die(mysql_error());
        $result_answers_count = mysql_fetch_array($result_answers_count);
}

 
       $result_current_answers_count = mysql_query("SELECT count(*) as count FROM Answers WHERE area_id = ".$area_id." AND question_id = '".$question_id."'")
		or die(mysql_error());
        $result_current_answers_count = mysql_fetch_array($result_current_answers_count);

# Get the Interview description text for the header
$result = mysql_query("SELECT * FROM ExpertiseArea WHERE area_id = $area_id")
		or die(mysql_error());
$myrow = mysql_fetch_array($result);
$area_text = stripcslashes($myrow["area_text"]);

# Get the question...
$result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id AND id = $question_id 
        ORDER BY question_id")
        or die(mysql_error());
$myrow = mysql_fetch_array($result);

# Find out which question we're on.
$sort_order = $myrow["question_id"];




# Get internal db id of the question and the help type
$question_db_id = $myrow["id"];
$help_type = $myrow["help"];
$help_text = $myrow["help_text"];

# Figure out approximatly what percentage complete we are at this particular question... gets displayed at the bottom of the page.
$percent_complete = round(100 * ($sort_order/$num_rows));

#If the question should be carried forward, store all the text in $carry_forward_text
if ($myrow["carry_forward"] == 1) {
	# Write this carry forward item to the database with the current session_id.
	$question_text = addslashes($myrow["question_text"]);
	$sql2 = "INSERT INTO carry_forward_temp (session_id,area_id,question_text) VALUES (\"$session_id\",$area_id,\"$question_text\")";
	
        #print "Tried to write to the temp_db: $sql2<br>\n";
        $result2 = mysql_query($sql2);
        
        #echo mysql_errno() . ": " . mysql_error() . "\n";
}	

include("$app_path/noncore/header.php");

if($permission_text!='')
 {
	echo $permission_text;
 }
else 
 {
	if($result_current_answers_count[count]==0)
	 {
	 	$finish_code = monitor_insert_result($area_id,$result_question_page[id],$result_question_page[question_id],$answer_id,$sort_order,1);
	 }
	else if($result_answers_count[count]>1 || $result_question_page[question_id]>$sort_order)
	 {
	 monitor_insert_result($area_id,$result_question_page[id],$result_question_page[question_id],$answer_id,$sort_order);
	 }
?>

<!-- The title area, with or without help and more tools links -->
<div style="border-bottom: 1px solid grey">
        <h3 align="left"><?php echo(_QUESTION) ?>: <?php  echo(stripslashes($area_text)) ?></h3>
<!-- 	<div style="text-align: right;">
		<?php # if ($myrow["help"] == _BASIC_HELP)
                      #          printf("<a href=\"#\" onclick=\"javascript:basic_help(%s);\">%s</a>", $question_db_id, _NEED_HELP);
                      #  else if ($myrow["help"] == _ADVANCED_HELP)
                      #          printf("<a href=\"#\" onclick=\"javascript:advanced_help(%s, %s, %s);\">%s</a>", $question_db_id, $question_id, $area_id, _NEED_HELP);
		?>
		&nbsp;&nbsp;&nbsp;
		<a href="#TB_inline?height=300&width=300&inlineId=moreTools" class="thickbox"><?php echo(_MORE_TOOLS) ?></a>
	</div> -->
</div>

<!-- The help area 
<div id="help-wrapper" style="float:right; width:400px; height:300px; text-align:right;">
	<div id="help-area" style="display:none; background:#ffff99;">
		<div>
		<div  style="text-align:center"> 
		<br /> -->
		<!-- print Basic Help if available -->
			<?php  #if ($myrow["help"] == _BASIC_HELP){
				#print("<div id='ajax-help-area'></div>");
			#} ?>
			<!-- print Advanced Help if available -->
			<?php #if ($myrow["help"] == _ADVANCED_HELP){
				#print("<div id='ajax-help-area'></div>");
			#} ?>
<!--		</div>
		<br />
		<a href="#" onclick="Effect.SlideUp('help-area'); return false;"><u><?php #echo(_HIDE_HELP) ?></u></a>
		</div>
	</div>
</div>
-->

<!-- The tools area, if available -->
<!-- <div id="moreTools" style="display: none;">
<h3>This is a sample tool</h3>
<p>
We could put there diagnostic tools, scripts to execute on client computer, etc.
</p>
</div> -->

<script>
	jQuery( document ).ready(function() {
		jQuery( ".question_id" ).click(function() {
			jQuery("#question_id").val(jQuery(this).attr("question_id"));
			jQuery("#answer_id").val(jQuery(this).attr("answer_id"));
			jQuery("#expert_wizard_form").submit();

			return false;
		});
	});
</script>
<?
 $crypt_key_subkey = generateCode(12);
 $crypt_key_question = generateCode(12);
 $crypt_key = $crypt_key_subkey.crypt_key($crypt_key_question,$crypt_key_subkey);
?>
<form action="expert_wizard.php?area_id=<?=$area_id;?>" id="expert_wizard_form" method="POST">
<input type="hidden" name="code" value="<?=$crypt_key?>">
<input type="hidden" name="question_id" id="question_id" value="" />
<input type="hidden" name="answer_id" id="answer_id" value="" />
<input type="hidden" name="content_id" value="<?=crypt_key($question_db_id,$crypt_key_question)?>">
</form>
<table>
 <tr><td>
<?php  
	printf("<br /><div id=\"%s\">%s</div><br />\n", 'question_' . $question_db_id, stripslashes($myrow["question_text"]));
	
	if($finish_code!="")
	 {
	echo '<center>'._YOUR_STAMP_IS.': <h2 style="align-text:center;">'.$finish_code.'</h2></center>';
	 }

	$result = mysql_query("SELECT * FROM Answers WHERE area_id = $area_id AND question_id = $question_id ORDER BY ans_id")
        or exit();
	
	# define new array containing answers internal database id and help text
	$answers_db_id = array();
	
    while ($myrow = mysql_fetch_array($result)) {
		# Event to handle on Mouse hovering an element : by default nothing (if No Help or Basic Help), unless Advanced Help is provided
		$event = "";
		if ($help_type == _ADVANCED_HELP)
			$event = "onMouseOver='mouseOver(" . $myrow["id"] . ");' onMouseOut='mouseOut(" . $myrow["id"] . ");'";
		$next_question_id = $myrow["next_question_id"];
		$some_question = 1;


		# echo answers
echo "<div id=\"answer" . ($help_type == _ADVANCED_HELP ? "_".$myrow["id"]:"")."\" ".$event." style=\"width: 100%;\"><TABLE border=\"0\" style=\"width: 100%;\" cellspacing=\"0\" cellpadding=\"0\">
				<TD WIDTH=\"6\"><IMG SRC=\"images/button_sx.gif\" WIDTH=\"6\" HEIGHT=\"40\" BORDER=\"0\"></TD>
				<TD ALIGN=center>
					<a href=\"expert_wizard.php?area_id=".$area_id."\" answer_id=\"".crypt_key($myrow["id"],$crypt_key_question)."\" question_id=\"".crypt_key($myrow["next_question_id"],$crypt_key_question)."\" class=\"question_id\">".stripslashes($myrow["answer_text"])."</a></td>
				<TD WIDTH=\"6\"><IMG SRC=\"images/button_dx.gif\" WIDTH=\"6\" HEIGHT=\"40\" BORDER=\"0\"></TD>
			</TABLE></div><br>";


/*
		printf("<div id=\"%s\" %s style=\"width: 100\%;\"><TABLE border=\"0\" style=\"width: 100\%;\" cellspacing=\"0\" cellpadding=\"0\">
				<TD WIDTH=\"6\"><IMG SRC=\"images/button_sx.gif\" WIDTH=\"6\" HEIGHT=\"40\" BORDER=\"0\"></TD>
				<TD ALIGN=center>
					<a href=\"expert_wizard.php?area_id=%s\" question_id=\"%s\" class=\"question_id\">%s</a></td>
				<TD WIDTH=\"6\"><IMG SRC=\"images/button_dx.gif\" WIDTH=\"6\" HEIGHT=\"40\" BORDER=\"0\"></TD>
			</TABLE></div><br>", 'answer' . ($help_type == _ADVANCED_HELP ? "_".$myrow["id"]:""), $event, $area_id, , );
*/
	}
?>	
</td></tr>
</table>
<?
if($help_text!="" && $settings[help_text]==true)
 {
echo '<script type="text/javascript" src="js/info/info.js"></script>

<a href="#" class="popup_info">'._ADVANCED_HELP_TEXT.'</a><div style="display:none">'.$help_text.'</div>';
 }
?>
	<hr style="clear: both;"/>

<?
        
        if ($some_question == 1) {  # if there are questions then put link to carry forward text on page
		# Link to carry forward notices
		if($carry_forward_text == 1) {
			print("<p><b><a href=\"follow_up.php?area_id=$area_id&session_id=$session_id\"
                              onclick=\"window.open(this.href,'window','width=900,height=480,resizable,scrollbars,toolbar,menubar');return false;\">
				"._ITEMS_TO_FOLLOW_UP_ON."</a></b></p>\n");
		}
	} else { # else print the list of carry forward items on the page.
		if($carry_forward_text == 1) {
			print("<h1><a href=\"follow_up.php?area_id=$area_id&session_id=$session_id\">
				"._EMAIL_OR_PRINT." "._ITEMS_TO_FOLLOW_UP_ON."</a></h1>\n");
		}  
	}
	
	# Output the percentage of the questions that the person has completed.
	print("<p>"._YOU_APPORXIMATELY." " . $percent_complete . "% "._YOU_APPORXIMATELY_COMPLETE."</p>");

	# Go Back and Start Over Again Link
	print("<p><b>" . _GOBACK . "&nbsp;or&nbsp;<a href=\"expert_wizard.php?area_id=$area_id\">"._START_OVER."</a></b></p>");
 }
	
include("$app_path/noncore/footer.php");
?>
      
