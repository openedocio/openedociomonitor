<?php
# include selected language
## include_once("../../languages/lang-french.php");

# include versionning information
require_once( '../includes/version.php' );

# retrieve settings from GET
$DBhostname = $_POST["DBhostname"];
$DBusername = $_POST["DBusername"];
$DBpassword = $_POST["DBpassword"];
$DBname = $_POST["DBname"];
$DBSample = $_POST["DBSample"];
$testing = $_POST["testing"];
$load_sql = $_POST["load_sql"];

# default settings if not set
if ($DBhostname == null) $DBhostname = 'localhost';
if ($DBusername == null) $DBusername = 'user';
if ($DBpassword == null) $DBpassword = 'password';
if ($DBname == null) $DBname = 'openexpert';
if ($DBSample == null) $DBSample = 1;
if ($testing == null) $testing = 0;
if ($load_sql == null) $load_sql = 0;

# flags
$connValid = 0;
$dbValid = 0;
$errmsg = '';
$success = false;
$writeResult = 0;

# test db connection settings
if ($testing == 1){
	# launch database connection check
	$link = @mysql_pconnect($DBhostname, $DBusername, $DBpassword);
	
	# if resource valid
	if($link){
		# set connection flag to valid
		$connValid = 1;
		
		# then test database name
		$db_selected = @mysql_select_db($DBname, $link);
		
		if ($db_selected){
			# set database name flag to valid
			$dbValid = 1;
		}		
	}	
}

// fill database and write data to noncore/config_inc.php
if ($load_sql == 1){
	# include utils
	include ("../includes/database_inc.php");

	# set file to import (with or without sample data)
	if ($DBSample == 1){
		$filename = '../sql/openexpert.sql';
	} else {
		$filename = '../sql/openexpert-empty.sql';
	}
	
	# import tables
	$success = mysql_import_file($filename, $errmsg);
	
	# if import succeed, then write db parameters to noncore/config_inc.php
	if ($success){
		# form text to write
		$text = "<?php \n\n" .
				"##############################################################################\n" .
				"# This is the installation file generated from the install wizard \n" .
				"# If something as gone wrong and you need to set parameters manually, \n" .
				"# you should better edit config_inc-default.php (which as a valid syntax) \n" .
				"# and rename it to config_inc.php. \n" . 
				"##############################################################################\n\n" .
				"# Database settings.  Please replace with settings for your mysql database server. \n" .
				"\$db_host = '$DBhostname'; \n" .
				"\$db_user = '$DBusername'; \n" .
				"\$db_password = '$DBpassword'; \n" .
				"\$db_name = '$DBname';  \n\n";

		# open noncore/config_inc.php et set size to 0
		$f = "../../noncore/config_inc.php";
		$handle = fopen($f, "w+");

		# write data to config_inc.php
		if (is_writable($f)) {
		    if (fwrite($handle, $text) === FALSE) {
				$writeResult = 'An error occured while writting data in ' . $fp;
		    } else {   
			    $writeResult = 1;   
				fclose($handle);
			}               
		}
		else {
		    $writeResult = $f . 'is not writeable';
		} 
	}
}

?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Open expert [<?php echo($_VERSION->_RELEASE) ?>] Web Installer</title>
	
	<meta http-equiv="content-style-type" content="text/css" />
	<link rel="stylesheet" type="text/css" href="../style/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../style/install.css" media="screen" />
	<link rel="shortcut icon" href="../style/img/openexpert.ico" type="image/x-icon" />
	
	<!-- prototype and scriptaculous -->
	<script src="../../js/prototype.js" type="text/javascript"></script>
	<script src="../../js/scriptaculous.js" type="text/javascript"></script>
	
	<script type="text/javascript">		
		function check() {
			// we check that the fields are correctly filled
			var paramValid=false;

			if ( $('DBhostname').value == '' ) {
				alert('Please enter a server name.');
				$('DBhostname').style.background="#ff9999";
				$('DBhostname').focus();
				paramValid=false;
			} else if ( $('DBusername').value == '' ) {
				alert('Please enter a mysql user name.');
				$('DBusername').style.background="#ff9999";
				$('DBusername').focus();
				paramValid=false;
			} else if ( $('DBname').value == '' ) {
				alert('Please enter a database name');
				$('DBname').style.background="#ff9999";
				$('DBname').focus();
				paramValid=false;
			} else {
				//alert('Parameters are valid ... Checking connection now.');
				// set testing flag to "1"
				$('testing').value = 1;
				// set hidden parameters of the form
				$('hidden-DBhostname').value = $('DBhostname').value;
				$('hidden-DBusername').value = $('DBusername').value;
				$('hidden-DBpassword').value = $('DBpassword').value;
				$('hidden-DBname').value = $('DBname').value;
				if ($('DBSample').checked == true){
					$('hidden-DBSample').value = 1;
				} else {
					$('hidden-DBSample').value = 0;
				}

				paramValid=true;
			}
			
			return paramValid;
		}
		
		function showErrmsg(){
			alert("Server returned the following error :\n" + <?php echo(($errmsg=='')?'""':'"'.$errmsg.'"') ?>);
		}
	</script>
</head>
<body>

<div id="global">
	<div id="entete">
		<h1>Welcome to OpenExpert Installation !!!</h1>
		<br />
		<p class="sous-titre">
			<img alt="" src="../style/img/openexpert-logo.gif" />
			
			<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
			<!--[if IE]><h3 style="padding-top: 10px">You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3><![endif]-->
			<comment><h3>You are installing Open<span style="color:red;">Expert</span> <u><?php echo($_VERSION->_RELEASE) ?></u>, which is a <?php echo($_VERSION->_DEV_STATUS) ?> release.</h3></comment>
			
		</p>
		<p style="clear: both;">
			<h3><strong>Follow the steps bellow to complete installation :</strong></h3>
		</p>
	</div>

	<div id="centre">
		<div id="navigation">
			<div>
				<h3>Licence</h3>
			</div>
			<div>
				<h3>Permissions</h3>
			</div>
			<div>
				<h2><u>Database Settings</u></h2>
			</div>
			<div>
				<h3>Application Settings</h3>
			</div>
			<div>
				<h3>Branding</h3>
			</div>
			<div>
				<h3>Installation complete</h3>
			</div>
		</div>
		
		<!-- hack for correct IE positionning, since only IE supports conditionnal comments AND non-standard <comment> tag -->
		<!--[if IE]><div id="contenu" style="margin-top: -10px; padding-top: 0px;" ><![endif]-->
		<comment><div id="contenu"></comment>
			<h2 style="text-align: center;">Settings for database connection</h2>
			<h3>Enter the parameters in the fields bellow, then click the "Test connection" button to test them. If connection is OK, you will be able to fill database with tables (with or without sample data) and write settings to config file.</h3>
			
			<form method="post" action="install3.php" onsubmit="return check();">
				<table>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2">
						MySQL Server name
						<br/>
						<input type="text" id="DBhostname"value="<?php echo "$DBhostname"; ?>" />
					</td>
					<td>
						<em>'localhost' if local installation, else your mysql server name</em>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						MySQL User Name
						<br/>
						<input type="text" id="DBusername" value="<?php echo "$DBusername"; ?>" />
					</td>
					<td>
						<em>mysql account to access to this server</em>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Password
						<br/>
						<input type="text" id="DBpassword" value="<?php echo "$DBpassword"; ?>" />
					</td>
					<td>
						<em>password for this mysql account</em>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						Database name
						<br/>
						<input type="text" id="DBname" value="<?php echo "$DBname"; ?>" />
					</td>
					<td>
						<em>'openexpert' by default</em>
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" id="DBSample" value="1" <?php if ($DBSample == 1) echo 'checked="checked"'; ?> />
					</td>
					<td>
						<label for="DBSample">Install sample data</label>
					</td>
					<td>
						<em>Uncheck if you want a blank installation (usefull for production)</em>
					</td>
				</tr>
				</table>

				<br />
				<div style="text-align: center;">
					<input type="button" id="reset-btn" onClick="javascript:document.location='install3.php'" value="Initialize fields" />
					<input type="submit" id="test-conn" value="Test Connection" />
					<input type="submit" id="load-sql-btn" value="Fill database" disabled />
					<br /><br />
					<span id="conn-failed" style="display: none; color:red;">Warning !!! Connection failed. Please check that the parameters<br />are correct before launching another test.</span>
					<span id="load-sql-failed" style="display: none; color:red;">Warning !!! OpenExpert has not been able to load tables in the database.<br /><a href="" onClick="javascript:showErrmsg();return false;">Please click here to see server response details.</a></span>
					<span id="load-sql-succeed" style="display: none; color:green;">Congratulations !!! Upload of tables in the database completed successfully.<br />You can now click on the "Continue" button to go to next step.</span>
					<input type="hidden" id="testing" name="testing" value="<?php echo "$testing"; ?>" />
					<input type="hidden" id="load_sql" name="load_sql" value="<?php echo "$load_sql"; ?>" />
					<input type="hidden" id="hidden-DBname" name="DBname" value="<?php echo "$DBname"; ?>" />
					<input type="hidden" id="hidden-DBhostname"  name="DBhostname" value="<?php echo "$DBhostname"; ?>" />
					<input type="hidden" id="hidden-DBusername" name="DBusername"  value="<?php echo "$DBusername"; ?>" />
					<input type="hidden" id="hidden-DBpassword" name="DBpassword" value="<?php echo "$DBpassword"; ?>" />
					<input type="hidden" id="hidden-DBSample" name="DBSample" value="<?php echo "$DBSample"; ?>" />
				</div>
			</form>
			
			<div id="nav-bottom">
				<form action="install4.php" style="text-align: center;">
					<div id="nav-bottom-left"><a href="install2.php">&lt; Back to permissions</a></div>
					<div id="nav-bottom-right"><input type="submit" id="continue" value="Continue" disabled /></div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="pied">
		<p><?php echo($_VERSION->_URL) ?></p>
		<p><?php echo($_VERSION->_COPYRIGHT) ?></p>
	</div>
</div>

<script type="text/javascript">
	testing = <?php echo($testing) ?>;
	load_sql = <?php echo($load_sql) ?>;
	connValid = <?php echo($connValid) ?>;
	dbValid = <?php echo($dbValid) ?>;
	errmsg = <?php echo(($errmsg=='')?'""':'"'.$errmsg.'"') ?>;
	success = <?php echo(($success==true)?'true':'false') ?>;
	writeResult = "<?php echo($writeResult) ?>";
	
	// if we are checking db connection, output the result	
	if ((testing == 1) && (load_sql != 1)){
		if ((connValid == 1) && (dbValid == 1)){
			// lock valid fields
			$('DBhostname').disabled = true;
			$('DBusername').disabled = true;
			$('DBpassword').disabled = true;
			$('DBname').disabled = true;
			// alert user
			alert('Connection established - parameters are OK');
			// deactivate 'Test connection' button
			$('test-conn').disabled = true;
			// set load_sql flag to "1"
			$('load_sql').value = 1;
			// activate 'Fill database' button
			$('load-sql-btn').disabled = false;
		} else if ((connValid == 1) && (dbValid == 0)){
			// lock valid fields
			$('DBhostname').disabled = true;
			$('DBusername').disabled = true;
			$('DBpassword').disabled = true;
			// alert user
			alert('Connection to server established, but database name is invalid');
			// highlight invalid fields
			$('DBname').style.background="#ff9999";
			$('DBname').focus();
		} else{
			alert('Connection failed - parameters are incorrect');
			// show 'Wrong parameters / Connection not established' span
			Effect.Appear('conn-failed');
		}
	}
	
	// if we are filling database
	if (load_sql == 1){
		if ((success == true) && (writeResult == 1)) {
			// lock fields
			$('DBhostname').disabled = true;
			$('DBusername').disabled = true;
			$('DBpassword').disabled = true;
			$('DBname').disabled = true;
			// deactivate 'Test connection' and 'Initialize'  buttons
			$('test-conn').disabled = true;
			$('reset-btn').disabled = true;
			// show congratulations span and unlock 'Continue' button
			Effect.Appear('load-sql-succeed');
			$('continue').disabled = false;
		} else if (success == true){
			// lock fields
			$('DBhostname').disabled = true;
			$('DBusername').disabled = true;
			$('DBpassword').disabled = true;
			$('DBname').disabled = true;
			// set load-sql flag to "0"
			$('load_sql').value = 0;
			// alert user about write failure
			alert('OpenExpert has been able to load tables in the database, but an error occured while writing database settings to config file :\n' + writeResult  + '\nPlease try again.');
		} else {
			// set load-sql flag to "0"
			$('load_sql').value = 0;
			// show 'Database filling failed' span
			Effect.Appear('load-sql-failed');
		}
	}
</script>

</body>
</html>
