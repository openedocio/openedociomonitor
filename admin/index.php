<?php 
$section_type = "admin";
include("../config_inc.php");
include("security_inc.php"); 

db_conn()
    or die ("Cannot connect to server");
	
$add = $_POST["add"];
$area_text = addslashes($_POST["area_text"]);
$area_id = $_GET["area_id"];
$delete = $_GET["delete"];
$visible = $_GET["visible"];
$properties = $_GET["properties"];

if (isset($_COOKIE["purge_cf"])) {
	setcookie("purge_cf","1", time()+3600*24);
} else {
	purge_carry_forward_temp();
}

if ($add) {
	# Add new question into the ExpertiseArea table
	$sql = "INSERT INTO ExpertiseArea (area_text) VALUES ('$area_text')";
  	$result = mysql_query($sql);

  	if (!$result) {
		print ("ExpertiseArea query failed\n");
	}
	# Get the area_id of the newly created Interview...
	$area_id = mysql_insert_id();
	
	# Add the initial question for the interview...
	header ("Location: interview_add_question.php?area_id=$area_id");
	
} elseif ($delete == 1) {	
	# Put delete code here...
	$sql = "DELETE FROM ExpertiseArea WHERE area_id=$area_id";
  	$result = mysql_query($sql);
	
	$sql = "DELETE FROM Answers WHERE area_id=$area_id";
  	$result = mysql_query($sql);
	
	$sql = "DELETE FROM Questions WHERE area_id=$area_id";
  	$result = mysql_query($sql);
} elseif ($properties == 1) {
	# Redirect to Edit properties page.
	header ("Location: properties.php?area_id=$area_id&properties=$properties");
	exit;
} elseif ($visible == 1 || $visible == 2) {
	# make an interview visible - NOTE: $visible is letting us know the current state... need to flip it. visible = 1, hide = 2
	if ($visible == 1) {
		$visible = 2;
	} else {
		$visible = 1;
	}
		
	$sql = "UPDATE ExpertiseArea SET visible=$visible WHERE area_id=$area_id";
	$result = mysql_query($sql) 
	or die(mysql_error());
}

include("$app_path/noncore/header.php");
?>
<script>
function confirmDelete(delUrl) {
	if (confirm("<?php echo(_CONFIRMDELETE) ?>")) {
		document.location = delUrl;
	}
}
</script>

  <?php include("navigation_inc.php"); ?>
  <script type="text/javascript">

if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
 var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
 if (ieversion>=8)
  document.write("<p><b>This admin Tool may not work correctly with Internet Explorer 8. Please use <a href='http://firefox.com'>Firefox</a> for editing OpenExpert Interviews.</b></p>")
 else if (ieversion>=7)
  document.write("<p><b>This admin Tool may not work correctly with Internet Explorer 7. Please use <a href='http://firefox.com'>Firefox</a> for editing OpenExpert Interviews.</b></p>")
 else if (ieversion>=6)
  document.write("<p><b>This admin Tool may not work correctly with Internet Explorer 6. Please use <a href='http://firefox.com'>Firefox</a> for editing OpenExpert Interviews.</b></p>")
 else if (ieversion>=5)
  document.write("<p><b>This admin Tool may not work correctly with Internet Explorer 5. Please use <a href='http://firefox.com'>Firefox</a> for editing OpenExpert Interviews.</b></p>")
}
</script>
  <p>
<form method="post" action="index.php">
<font face="Arial, Helvetica, sans-serif"><?php echo(_NEWEXPERTISE) ?>:</font><br>
<input name="area_text" type="text" size="80">
<input type="submit" name="add" value="<?php echo(_ADD) ?>">
</form>

<p><strong>Visible Inverviews</strong></p>
<table border="2" width="100%">
  <?php  
     
    $result = mysql_query("SELECT * FROM ExpertiseArea WHERE visible=1 ORDER BY display_order")
        or exit();

    while ($myrow = mysql_fetch_array($result)) {
    	if ($myrow["visible"] == 1)
			$visible_text = _HIDE;
		else
			$visible_text = _SHOW;
		
		
		# if no photo available, replace with generic no photo
		printf("<tr><td><a href=\"interview_edit.php?area_id=%s\">%s</a></td><td>%s&nbsp;</td><td>%s&nbsp;</td>
			<td><a href=\"index.php?area_id=%s&properties=1\">%s</a></td><td><a href=\"export.php?area_id=%s\">%s</td><td><a href=\"index.php?area_id=%s&visible=%s\">%s</a></td>
			<td><a href=\"index.php?area_id=%s&delete=1\" onclick=\"return confirm('%s');\">
			%s</a></td></tr>\n",
			$myrow["area_id"],stripslashes($myrow["area_text"]),stripslashes($myrow["author"]),stripslashes($myrow["version"]),$myrow["area_id"],_PROPERTIES,
			$myrow["area_id"],_EXPORT,$myrow["area_id"],$myrow["visible"],$visible_text,$myrow["area_id"],_REALLYDELETE,_DELETE);
		
			
	}
?>	
</table>
<p><strong>Hidden Inverviews</strong></p>
<table border="2" width="100%">
  <?php  
    
    $result = mysql_query("SELECT * FROM ExpertiseArea WHERE visible=2 ORDER BY display_order")
        or exit();

    while ($myrow = mysql_fetch_array($result)) {
    	if ($myrow["visible"] == 1)
			$visible_text = "Hide";
		else
			$visible_text = "Show";
		
		# if no photo available, replace with generic no photo
		printf("<tr><td><font face=\"Arial, Helvetica, sans-serif\"><a href=\"interview_edit.php?area_id=%s\">%s</a></font></td><td>%s&nbsp;</td><td>%s&nbsp;</td>
			<td><a href=\"index.php?area_id=%s&properties=1\">%s</a></td><td><a href=\"export.php?area_id=%s\">%s</td><td><a href=\"index.php?area_id=%s&visible=%s\">%s</a></td>
			<td><font face=\"Arial, Helvetica, sans-serif\"><a href=\"index.php?area_id=%s&delete=1\" onclick=\"return confirm('%s');\">
			%s</a></font></td></tr>\n",
			$myrow["area_id"],stripslashes($myrow["area_text"]),stripslashes($myrow["author"]),$myrow["version"],$myrow["area_id"],_PROPERTIES,$myrow["area_id"],
			_EXPORT,$myrow["area_id"],$myrow["visible"],$visible_text,$myrow["area_id"],_REALLYDELETE,_DELETE);
			
	}
?>	
</table>

<?php
include("$app_path/noncore/footer.php");
?>