<?php

/**************************************************************************/
/* Constants used in the php code */
/**************************************************************************/

# Help related constants

	/**
	Doesn't show any help
	*/
define("_NO_HELP",0);
	/**
	Basic help
	*/
define("_BASIC_HELP",1);
	/**
	Advanced help
	*/
define("_ADVANCED_HELP",2);

?>