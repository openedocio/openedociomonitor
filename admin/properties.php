<?php 
$section_type = "admin";
include("../config_inc.php");
include("security_inc.php"); 
include_once("fckeditor/fckeditor.php") ;

db_conn()
    or die ("Cannot connect to server");
	
$area_id = $_GET["area_id"];
$properties = $_GET["properties"];
$save = $_POST["save"];
$return = $_POST["return"];

if ($save) {
	$area_text = addslashes($_POST["area_text"]);
	$author = addslashes($_POST["author"]);
	$description = addslashes($_POST["description"]);
	$location = addslashes($_POST["location"]);
	$version = addslashes($_POST["version"]);
	$notes = addslashes($_POST["notes"]);
	$language = addslashes($_POST["language"]); 
	
	# Use update query here to Save everything.
	$sql = "UPDATE ExpertiseArea SET area_text='$area_text',author='$author',description='$description',
		location='$location',version='$version',notes='$notes',language='$language' WHERE area_id=$area_id";
	$update_sql = mysql_query($sql);
	
	# Return to the Admin index page:
	header ("Location: index.php"); 
    exit;  
} elseif ($return) {
	# Return to the Admin index page:
	header ("Location: index.php"); 
    exit;  
}

include("$app_path/noncore/header.php");
?>

<div align="left">
  <?php include("navigation_inc.php"); ?>
  </div>
  <p>

<?php  
    
    $result = mysql_query("SELECT * FROM ExpertiseArea WHERE area_id=$area_id")
        or exit();

    $myrow = mysql_fetch_array($result)    	
?>

<form method="post" action="properties.php?area_id=<?php print $area_id ?>">

<table border="2" width="100%">
  <tr><td align="right">Interview Title</td><td><input name="area_text" type="text" value="<?php  echo stripslashes($myrow["area_text"]);  ?>" size="50" /></td></tr>
  <tr><td align="right">Author</td><td><input name="author" type="text" value="<?php  echo stripslashes($myrow["author"]);  ?>" size="30" /></td></tr>
  <tr><td align="right" valign="top">Description</td><td>
		<?php
        $oFCKeditor = new FCKeditor('description') ;
        $oFCKeditor->BasePath = 'fckeditor/' ;
        $oFCKeditor->ToolbarSet = 'Basic';
        $oFCKeditor->Height		= '100' ;
        $oFCKeditor->Value = stripslashes($myrow["description"]) ;
        $oFCKeditor->Create() ;
        ?>  
  
	</td></tr>
  <tr><td align="right">Location</td><td><input name="location" type="text" value="<?php  echo stripslashes($myrow["location"]);  ?>" size="30" /></td></tr>
  <tr><td align="right">Version</td><td><input name="version" type="text" value="<?php  echo stripslashes($myrow["version"]);  ?>" size="30" /></td></tr>
  <tr><td align="right" valign="top">Notes</td><td>
		<?php
        $oFCKeditor = new FCKeditor('notes') ;
        $oFCKeditor->BasePath = 'fckeditor/' ;
        $oFCKeditor->ToolbarSet = 'Basic';
        $oFCKeditor->Height		= '100' ;
        $oFCKeditor->Value = stripslashes($myrow["notes"]);
        $oFCKeditor->Create() ;
        ?>  
  
  </td></tr>
  <tr><td align="right">Language</td><td><input name="language" type="text" value="<?php  echo stripslashes($myrow["language"]);  ?>" size="50" /></td></tr>
</table>
<input name="save" value="Save" type="submit" /><input name="return" value="Return to Main Menu" type="submit" />
</form>

<?php
include("$app_path/noncore/footer.php");
?>

