<?php
# if noncore/config_inc.php exists (and is not empty), go back to application index.php
	
if (file_exists( '../noncore/config_inc.php' ) && filesize( '../noncore/config_inc.php' ) > 15) {
	
	print "File Exists: " . file_exists( '../noncore/config_inc.php' ) . "<br>\n";
	print "File Size: " . filesize( '../noncore/config_inc.php' ) . "<br>\n";
	
	#header( "Location: ../index.php" );
	exit();
}

# settings needed for language detection
include("../includes/utils.php");
 
#detect language
$allowed_langs = array ('en');
$lang = lang_getfrombrowser ($allowed_langs, 'en', null, false);

# redirect to correct language installation package
header( "Location: " . $lang . "/index.php" );
exit();

?>
