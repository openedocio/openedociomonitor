<?php
# Include user config
include("../config_inc.php"); 
db_conn()
    or die ("Cannot connect to server");

# Get area and question id, define $datas
$area_id = $_GET["area_id"];
$question_id = $_GET["question_id"];
$question_db_id = $_GET["question_db_id"];
$datas = array();


# ----------------------------------- Get the question help ---------------------------------
$result = mysql_query("SELECT * FROM QuestionsHelp WHERE question_db_id = $question_db_id")
		or exit();

# get the number of rows of result		
$num_rows = mysql_num_rows($result);

# if there is a help, add it to $datas, else add DEFAULT TEXT
if ($num_rows != 0){
	$myrow = mysql_fetch_array($result);
	$datas['question'] = $myrow["help_text"];
} else{
	$datas['question'] = _DEFAULT_ADV_HELP_TEXT;
}


# --------------------------------------- Get the answers helps -----------------------------------
$result = mysql_query("SELECT * FROM AnswersHelp WHERE area_id = $area_id 
		ORDER BY answer_id")
		or exit();

# create temporary array to store answers help
$answers_data = array();

# get the number of rows of result	
$num_rows = mysql_num_rows($result);

# retrieve help text for answers that have one	
if ($num_rows != 0){
	while ($myrow = mysql_fetch_array($result)) {
		$answers_data[$myrow['answer_id']] = $myrow['help_text'];
	}
}


# -------------------------------------------- Get the whole answers for this question and construct help array ------------------------------------
$result = mysql_query("SELECT * FROM Answers WHERE area_id = $area_id AND question_id = $question_id 
		ORDER BY id")
		or exit();
		
# create temporary array to store all answers and their help
$all_answers_data = array();

# get the number of rows of result	
$num_rows = mysql_num_rows($result);

# if there are answers, loop on them to find help for these answers (else, nothing to add, the user will never hover an answer)
if ($num_rows != 0){
	# while there is answers, retrieve help or add default 'no help' text
    while ($myrow = mysql_fetch_array($result)) {
		# if there is a corresponding help text, add it to array, else add default 'no help' text
		if ($answers_data[$myrow['id']]){
			$all_answers_data[$myrow['id']] = $answers_data[$myrow['id']];
		} else{
			$all_answers_data[$myrow['id']] = _DEFAULT_ADV_NO_HELP_TEXT;
		}
	}
}

# add 'answers' to $datas
$datas['answers'] = $all_answers_data;

# Return JSON
header("X-JSON: " . json_encode($datas));
?>