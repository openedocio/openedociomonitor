<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","UTF-8");
define("_HOME","Початок");
define("_FAQ","Інструкції");
define("_ABOUT","О нас");
define("_ADMIN","Вхід інструктора");
define("_AREASEXPERTISE","Частини");
define("_SPONSORS","Спонсори");
define("_FOLLOWUP","Детально");
define("_FAQLONG","Часті питання");
define("_QUESTION","Питання");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">Назад</a> ]");
define("_YOU_APPORXIMATELY","Ви закінчили");
define("_YOU_APPORXIMATELY_COMPLETE","приблизно");
define("_START_OVER","Почати спочатку");
define("_ITEMS_TO_FOLLOW_UP_ON","Подивитися детально");
define("_EMAIL_OR_PRINT","Відправити по Email чи роздрукувати");
define("_YOUR_STAMP_IS","Ваш код");

define("_INPUT_FINISH_CODE","Введіть отриманний код з попередньої частини");
define("_YOUR_CODE_WRONG","Ваш код невірний");

define("_FAQ_TEXT","<h1>Керівництво OpenExpert</h1><p>Прочитайте кожну сторінку чи зображення яке відображено вам. Зробить вибір з 5 запропонованних вам речень, по інформації яку ви прочли.<br /><br /><b>Будьте уважні при виборі вірного речення, тому що якщо ви зробите невірний вибір, ви будете переміщені на декалька шагів назад</b></p>");

define("_PLAY_AUDIO","Прослухати аудіо");
define("_PAUSE_AUDIO","Зупинити аудіо");
define("_INCREASE_VOLUME","Збильшити звук");
define("_DECREASE_VOLUME","Змешнити звук");

# Admin Text
define("_NEWEXPERTISE","Створити нову главу");
define("_ADMINHOME","Адміністрування додому");
define("_USERS","Користувачі");
define("_SETTINGS","Налаштування");
define("_ADD","Додати");
define("_SAVECHANGES","Зберегти зміни");
define("_CARRYFORWARD","Перейти до наступної сторінки");
define("_NEXTLINK","Наступне посилання");
define("_DELETE","Видалити");
define("_NEWQUESTION","Нове питання");
define("_NEWANSWER","Нова відповідь");
define("_LINKTO","Посилання на");
define("_CONFIRMDELETE", "Ви бажаєте видалити підручник?");
define("_ADMINLOGIN", "Адмін - вхід");
define("_USERNAME", "Ім'я користувача");
define("_PASSWORD", "Пароль");
define("_PASSWORDERROR", "ИІм'я користувача чи пароль не зівпадає");
define("_PROPERTIES","Властивості");
define("_HIDE","Сховати");
define("_SHOW","Відобразити");
define("_REALLYDELETE","Підтвердити видалення?");
define("_EXPORT","Експорт");
define("_IMPORT","Імпорт підручників");
define("_DOWNLOADFILE","Викачати як експортованний файл");


# Help Text
define("_NO_HELP_TEXT","Допомога відсутня");
define("_ADVANCED_HELP_TEXT","Натисніть для отримання підказки");
define("_NEED_HELP","Потрібна допомога?");
define("_MORE_TOOLS","Додаткові інструменти");
define("_HIDE_HELP","Сховати допомогу");
define("_NO_HELP_AVAILABLE","Допомога відсутня для цього елементу");
define("_DEFAULT_ADV_HELP_TEXT","Наведіть на питання для отримання допомоги");
define("_DEFAULT_ADV_NO_HELP_TEXT","Допомога відсутня для цієї відповіді");

?>