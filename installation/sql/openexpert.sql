SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";-- EOQ --

--
--  Table `Answers`
--

DROP TABLE IF EXISTS `Answers`;-- EOQ --
CREATE TABLE IF NOT EXISTS `Answers` (
  `id` int(11) NOT NULL auto_increment,
  `ans_id` int(11) default NULL,
  `question_id` int(11) default NULL,
  `next_question_id` int(11) default NULL,
  `answer_text` text,
  `area_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;-- EOQ --


-- --------------------------------------------------------

--
--  Table `AnswersHelp`
--

DROP TABLE IF EXISTS `AnswersHelp`;-- EOQ --
CREATE TABLE IF NOT EXISTS `AnswersHelp` (
  `id` int(11) NOT NULL auto_increment,
  `answer_id` int(11) default NULL,
  `help_text` text,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;-- EOQ --


-- --------------------------------------------------------

--
--  Table `ExpertiseArea`
--

DROP TABLE IF EXISTS `ExpertiseArea`;-- EOQ --
CREATE TABLE IF NOT EXISTS `ExpertiseArea` (
  `area_id` int(11) NOT NULL auto_increment,
  `area_text` text,
  `author` varchar(30) default NULL,
  `display_order` int(11) default NULL,
  `visible` tinyint(4) NOT NULL default '1',
  `description` varchar(250) default NULL,
  `location` varchar(40) default NULL,
  `version` varchar(10) default NULL,
  `notes` text,
  `language` varchar(20) default NULL,
  PRIMARY KEY  (`area_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;-- EOQ --


-- --------------------------------------------------------

--
--  Table `Questions`
--

DROP TABLE IF EXISTS `Questions`;-- EOQ --
CREATE TABLE IF NOT EXISTS `Questions` (
  `id` int(11) NOT NULL auto_increment,
  `question_id` int(11) default NULL,
  `area_id` int(11) default NULL,
  `question_text` text,
  `carry_forward` tinyint(4) default NULL,
  `help` int(11) default '0' COMMENT 'aide',
  `help_text` TEXT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;-- EOQ --


-- --------------------------------------------------------

--
--  Table `QuestionsHelp`
--

DROP TABLE IF EXISTS `QuestionsHelp`;-- EOQ --
CREATE TABLE IF NOT EXISTS `QuestionsHelp` (
  `id` int(11) NOT NULL auto_increment,
  `question_db_id` int(11) default NULL,
  `help_text` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;-- EOQ --


-- --------------------------------------------------------

--
--  Table `Version`
--

DROP TABLE IF EXISTS `Version`;-- EOQ --
CREATE TABLE IF NOT EXISTS `Version` (
  `major` varchar(3) NOT NULL,
  `minor` varchar(3) NOT NULL,
  `revision` varchar(3) default NULL,
  `date` varchar(8) NOT NULL,
  `svn_build` varchar(6) default NULL,
  `name` varchar(50) default NULL,
  `dev_status` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ;-- EOQ --

--
-- Table 'carry_forward_temp'
--

DROP TABLE IF EXISTS `carry_forward_temp`;-- EOQ --
CREATE TABLE IF NOT EXISTS `carry_forward_temp` (
  `id` int(11) NOT NULL auto_increment,
  `session_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `session_id` varchar(50) default NULL,
  `area_id` int(11) NOT NULL,
  `question_text` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `session_id` (`session_id`,`area_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;-- EOQ --


--
-- Table 'monitor'
--
DROP TABLE IF EXISTS `monitor`;-- EOQ --
CREATE TABLE IF NOT EXISTS `monitor` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NULL DEFAULT '0',
	`guest_id` CHAR(255) NULL DEFAULT '0',
	`area_id` INT(11) NULL DEFAULT '0',
	`question_id` INT(11) NULL DEFAULT '0',
	`answer_id` INT(11) NULL DEFAULT '0',
	`status` INT(11) NULL DEFAULT '0',
	`date` INT(11) NULL DEFAULT '0',
	`finish_code` CHAR(50) NULL DEFAULT NULL,
	`ip` CHAR(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;-- EOQ --