<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","UTF-8");
define("_HOME","Start");
define("_FAQ","Instructions");
define("_ABOUT","About");
define("_ADMIN","Instructor Login");
define("_AREASEXPERTISE","Units");
define("_SPONSORS","Sponsors");
define("_FOLLOWUP","Items to follow up on");
define("_FAQLONG","Frequently Asked Questions");
define("_QUESTION","Page");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">Go Back</a> ]");
define("_YOU_APPORXIMATELY","You are approximately");
define("_YOU_APPORXIMATELY_COMPLETE","complete");
define("_START_OVER","Start Over");
define("_ITEMS_TO_FOLLOW_UP_ON","Items To Follow Up On");
define("_EMAIL_OR_PRINT","Email or Print");
define("_YOUR_STAMP_IS","Your Stamp is");
define("_FAQ_TEXT","<h1>Doing an Edocio</h1><p>Read each page or screen as presented to you. Then select from the 5 sentences presented. Select the one contained in the page or screen you just read. <br /><br /><b>Take care to select the correct sentence because choosing a sentence not contained in the page you just read will send you one or more back not forward. </b></p>");
define("_HEADER_TITLE","<h2>The Role of Philosophy<br>in the Evolution of Western Culture</h2><h3>By Wallace H Provost Jr.</h3>");

define("_INPUT_FINISH_CODE","Input a finish code from a previous unit");
define("_YOUR_CODE_WRONG","Your code is wrong!");



define("_PLAY_AUDIO","Play the Audio");
define("_PAUSE_AUDIO","Pause the Audio");
define("_INCREASE_VOLUME","Increase Volume");
define("_DECREASE_VOLUME","Decrease Volume");



# Admin Text
define("_NEWEXPERTISE","Create a New Chapter");
define("_ADMINHOME","Admin Home");
define("_USERS","Users");
define("_SETTINGS","Settings");
define("_ADD","Add");
define("_SAVECHANGES","Save Changes");
define("_CARRYFORWARD","Carry Forward");
define("_NEXTLINK","Next Link");
define("_DELETE","Delete");
define("_NEWQUESTION","New Q");
define("_NEWANSWER","New A");
define("_LINKTO","Link To");
define("_CONFIRMDELETE", "Are you sure you want to delete this testbook?");
define("_ADMINLOGIN", "Admin Login");
define("_USERNAME", "User Name");
define("_PASSWORD", "Password");
define("_PASSWORDERROR", "The username and password you provided do not match.");
define("_PROPERTIES","Properties");
define("_HIDE","Hide");
define("_SHOW","Show");
define("_REALLYDELETE","Really Delete?");
define("_EXPORT","Export");
define("_IMPORT","Import Textbooks");
define("_DOWNLOADFILE","Download this as an export file");


# Help Text
define("_NO_HELP_TEXT","No help available");
define("_ADVANCED_HELP_TEXT","Click to get help");
define("_NEED_HELP","Need Help ?");
define("_MORE_TOOLS","More tools");
define("_HIDE_HELP","Hide help");
define("_NO_HELP_AVAILABLE","No help available for this item");
define("_DEFAULT_ADV_HELP_TEXT","Hover an answer to get help about it");
define("_DEFAULT_ADV_NO_HELP_TEXT","No help is available for this answer");

?>
