<?
	/**
	* Mysql Connection
	*/
 class mysql_database_connection
  {
	   /**
	    * Host
	    * @var string the host
	    */
	var $host;
	   /**
	    * User
	    * @var string the user
	    */
	var $user;
	   /**
	    * Password
	    * @var string the password
	    */
	var $password;
	   /**
	    * Database
	    * @var string the database
	    */
	var $database;
	   /**
	    * Query
	    * @var string the query
	    */
	Var $query;
	   /**
	    * If debug mode is true, we don't show errors
	    * @var boolean the debug
	    */
	Var $debug;
	   /**
	    * Execution time
	    * @var float seconds
	    */
	var $time;

	/**
	* Constructor of class
	*/
	function __construct()
	 {
		global $db_host, $db_user, $db_password, $db_name;

			$this->localhost = $db_host;
			$this->debug = 0;
			$this->host = $host;
			$this->user = $db_user;
			$this->password = $db_password;
			$this->database = $db_name;
		mysql_connect($this->host, $this->user, $this->password);
		
		mysql_select_db($this->database);
	 }

	/**
	* Select data and put it to an array
	* @param string $query Query
	* @return array Rows
	*/
	function Select($query)
	 {
	$this->query = $query;

		$time_start = microtime(true);

		$result = MYSQL_QUERY($query);
		$number = MYSQL_NUMROWS($result);

				$i = 0;
				while($row = mysql_fetch_assoc($result))
				 {
					$rows[$i] = $row;
					$i++;
				 }

		mysql_free_result($result);

		$rows[count] = $number;

		$time_end = microtime(true);
		$this->time = $time_end - $time_start;

		$this->Error();

		return $rows;	
	 }

	/**
	* Update data
	* @param string $query Query
	*/
	function Update($query)
	 {
		$time_start = microtime(true);

		$this->query = $query;
		MYSQL_QUERY($query);
		$this->Error();

		$time_end = microtime(true);
		$this->time = $time_end - $time_start;
	 }

	/**
	* Insert data
	* @param string $query Query
	*/
	function Insert($query)
	 {
		$time_start = microtime(true);

		$this->query = $query;
		MYSQL_QUERY($query);
		$this->Error();

		$time_end = microtime(true);
		$this->time = $time_end - $time_start;
	 }

	/**
	* Delete data
	* @param string $query Query
	*/
	function Delete($query)
	 {
		$time_start = microtime(true);

		$this->query = $query;
		MYSQL_QUERY($query);
		$this->Error();

		$time_end = microtime(true);
		$this->time = $time_end - $time_start;
	 }

	/**
	* Show errors if it has
	*/
	function Error()
	 {
			if($this->debug == 1)
			if(mysql_error())
	 		 {
			$form = "<br><br><table border=0><tr><td><font color=#FF0000>".mysql_errno().mysql_error()."</font></td></tr></table></br>";
			print $this->query.'<br />';
		 	 }


	 }

  }
?>