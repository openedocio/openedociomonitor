<?php
	/**
	 * Check permissions to access to the Unit
	 * If an user doesn't have access, he has a form to input the code
	 * @param integer $area_id the ID of the Unit
	 * @return HTML Form to input the code 
	 */
function permission_unit($area_id)
 {
	global $mysql,$settings;

	if($settings[unit_code]==true)
	 {
		if($_POST[finish_code]!='') $finish_code = $_POST[finish_code];

		if($finish_code=='' && $_SESSION[finish_code]!='') $finish_code = $_SESSION[finish_code];

			$result_area = $mysql->Select("SELECT area_id,area_text FROM ExpertiseArea WHERE area_id<'".$area_id."' && visible=1 ORDER BY area_id DESC LIMIT 1");

				if($result_area[count]>0)
				 {
					if($finish_code!='')
					 $result = $mysql->Select("SELECT * FROM monitor WHERE finish_code='".$finish_code."' && area_id='".$result_area[0][area_id]."' LIMIT 1");
				
					if($result[count]==0 || $finish_code=='')
					 {
						if($_POST[finish_code]!='') $error_code = '<b>'._YOUR_CODE_WRONG.'</b>';
					 $form = $form.'<h1>'._INPUT_FINISH_CODE.' "'.$result_area[0][area_text].'":</h1>'.$error_code.'<form action="" method="post"><input type="text" size="50" name="finish_code" /> <input type="submit" name="submit" value="submit" /></form>';
					 }
					else
					 $_SESSION[finish_code] = $finish_code;
				 }

	return $form;
	 }
 }

	/**
	* Convert number to char
	* @param string $number
	* @return string
	*/
function toChars($number)
  {
   $res = base_convert(microtime().$number, 10,26);
   $res = strtr($res,'0123456789','qrstuvxwyz');
   return $res;
  }

	/**
	 * Write statistics
	 * It writes all steps of unit except "next"
	 * @param integer $area_id the ID of the Unit
	 * @param integer $question_id the ID of Question
	 * @param integer $question_pos the Position of Question
	 * @param integer $answer_id the ID of Answer
	 * @param integer $answer_pos the Position of Answer
	 * @param integer $finish the Finish step of Questions
	 * @return string the finish code for identification user in statisctic and to get access to next Unit
	 */

function monitor_insert_result($area_id,$question_id,$question_pos,$answer_id,$answer_pos,$finish=0)
  {
	global $mysql;

		$session_id = session_id();

	$result_area = $mysql->Select("SELECT * FROM ExpertiseArea WHERE area_id = '".$area_id."' LIMIT 1");

	if($result_area[count]>0)
	 {
		$result = $mysql->Select("SELECT count(*) as count FROM monitor WHERE date>'".(time()-86400)."' && guest_id='".$session_id."' && area_id='".$area_id."' && question_id='".$question_id."' && answer_id='".$answer_id."'");

				 $finish_code = "";
	
		if($result[0][count]==0 || $finish=="1")
		 {
			if($finish==1)
			 {
				$result_finish = $mysql->Select("SELECT finish_code FROM monitor WHERE date>'".(time()-86400)."' && guest_id='".$session_id."' && area_id='".$area_id."' && finish_code!=''");

				if($result_finish[0][finish_code]!="") $finish_code = $result_finish[0][finish_code];

				if($finish_code=="")
				 $finish_code = toChars(ip2long($_SERVER['REMOTE_ADDR']));
		
			 }
		
				if($result_finish[0][finish_code]=="")
				 {
					if($question_id==0) $status = 2;
					else if($answer_pos>$question_pos) $status = 1; else $status = 0;
					mysql_query("INSERT INTO monitor(guest_id,area_id,question_id,answer_id,status,date,finish_code,ip) VALUES('".$session_id."','".$area_id."','".$question_id."','".$answer_id."','".$status."','".time()."','".$finish_code."','".$_SERVER[REMOTE_ADDR]."')");
				 }
		 }
	 }
	return $finish_code;
  }

	/**
	* Generate a random code;
	* @param integer $length code
	* @return string a random code
	*/

function generateCode($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

	/**
	* Crypt a key
	* @param string $input a code
	* @param string $key a key for code
	* @param boolean $decode trigger to encrypt and decrypt the code
	* @return string a encrypted or a decrypted code
	*/

function crypt_key($input,$key,$decode=false)
 {

$key = $key.$session_id;

 $rc4 = new Crypt_RC4;
 $rc4->setKey($key);

if($decode==false)
 return $rc4->crypt($input);
else 
 return $message = $rc4->decrypt($input);
 }

	/**
	* Browswer language detection, refer to Christian Seiler http://.de.selfhtml.org/
	* detect browser language
	* @param array $allowed_languages allowed languages
	* @param string $default_language default language
	* @param null $lang_variable language variable
	* @param boolean $strict_mode strict mode
	* @return string a current language;
	*/
function lang_getfrombrowser ($allowed_languages, $default_language, $lang_variable = null, $strict_mode = true) {
         // use $_SERVER['HTTP_ACCEPT_LANGUAGE'] in case of lang_variable is not set
         if ($lang_variable === null) {
                 $lang_variable = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
         }


         if (empty($lang_variable)) {
                 // lang_variable is empty, return default language
                 return $default_language;
         }

         // header splittting
         $accepted_languages = preg_split('/,\s*/', $lang_variable);

         // set default language
         $current_lang = $default_language;
         $current_q = 0;

         // iterate over all languages
         foreach ($accepted_languages as $accepted_language) {
                 // extrect all information
                 $res = preg_match ('/^([a-z]{1,8}(?:-[a-z]{1,8})*)'.
                                    '(?:;\s*q=(0(?:\.[0-9]{1,3})?|1(?:\.0{1,3})?))?$/i', $accepted_language, $matches);

                 // valid syntax?
                 if (!$res) {
                         // no?, ignore
                         continue;
                 }

                 // extract language code and split
                 $lang_code = explode ('-', $matches[1]);

                 // do we have a quality setting?
                 if (isset($matches[2])) {
                         // use this quality
                         $lang_quality = (float)$matches[2];
                 } else {
                         // assumem quality = 1
                         $lang_quality = 1.0;
                 }

                 // do until language code is emty
                 while (count ($lang_code)) {
                         // do we have our language code?
                         if (in_array (strtolower (join ('-', $lang_code)), $allowed_languages)) {
                                 // determine quality
                                 if ($lang_quality > $current_q) {
                                         // use this language
                                         $current_lang = strtolower (join ('-', $lang_code));
                                         $current_q = $lang_quality;
                                         // leave inner while
                                         break;
                                 }
                         }
                         // stict mode, don't minmalize
                         if ($strict_mode) {
                                 // break inner while
                                 break;
                         }
                         // cut the language code
                         array_pop ($lang_code);
                 }
         }
         // return language
         return $current_lang;
 }
 
	/**
	* Function to Validate Email addresses
	* @param string $email Email
	* @return integer true or false
	*/

function validate_email($email)
{

   // Create the syntactical validation regular expression
   $regexp = "^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$";

   // Presume that the email is invalid
   $valid = 0;

   // Validate the syntax
   if (eregi($regexp, $email))
   {
      list($username,$domaintld) = split("@",$email);
      // Validate the domain
      if (getmxrr($domaintld,$mxrecords))
         $valid = 1;
   } else {
      $valid = 0;
   }

   return $valid;

}




	/**
	* Outputs question table on the right side of the interview_edit.php page.
	* @param integer $area_id the ID of the Unit
	* @param integer $question_id the ID of Question
	* @param integer $hide_answers hide question
	*/

function interview_print_questions($area_id,$question_id,$hide_answers)
{
         $result = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
                  or exit();
                 
         while ($myrow = mysql_fetch_array($result)) {
                  $question_id = $myrow["id"];
                  if ($myrow["carry_forward"] == 1) {
                           $carry_forward = "checkbox_1.png";
                  } else {
                           $carry_forward = "checkbox_0.png";
                  }
                          
                  #Prints the Question Text
                  printf("<tr><td valign=\"top\"><a name=\"%s\">%s.</a></td><td colspan=\"2\" width=\"430px\">%s</td><td valign=\"top\">
                           <img src=\"../images/%s\"></td><td></td>
                           <td valign=\"top\"><input type=\"button\" name=\"edit_question\" id=\"edit_question\"
                          value=\"Edit\" onclick=\"javascript:edit_question(%s,%s);\" /></td></tr>\n",
                           $myrow["id"],$myrow["question_id"],stripslashes($myrow["question_text"]),
                           $carry_forward,$area_id,$myrow["id"]);
                          

                  
                  # Output the answer fields...
                  if ($hide_answers == 1) {
                           #nothing
                  } else {
                           # This builds the answer fields
                           $sql = "SELECT * FROM Answers WHERE area_id = $area_id AND question_id = $question_id ORDER BY ans_id";
                           #print "sql: $sql<br>\n";
                           $result2 = mysql_query($sql)
                                    or exit();
                           
                           while ($myrow2 = mysql_fetch_array($result2)) {	
                                    $next_question_id = $myrow2["next_question_id"];
                                    $result4 = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id AND id = $next_question_id")
                                    or exit;
                                    $myrow4 = mysql_fetch_array($result4);
                                    $next_question_id_val = $myrow4["question_id"];                           
                                    $ans_id = $myrow2["ans_id"];
                                    printf("<tr><td>&nbsp;</td><td>&nbsp;&nbsp;- %s</td><td>&nbsp;</td><td valign=\"top\"></td>
                                             <td>%s</td></tr>\n",stripslashes($myrow2["answer_text"]),$next_question_id_val);
                           }
                  }
         }
}

	/**
	* Prints out the Answers on the left side of the interview_edit.php page.
	* @param integer $area_id the ID of the Unit
	* @param integer $question_id the ID of Question
	*/
function interview_print_answers($area_id,$question_id)
{
         # This builds the answer fields
         $result2 = mysql_query("SELECT * FROM Answers WHERE area_id = $area_id AND question_id = $question_id ORDER BY ans_id")
                  or exit();
         
         print "<ul id=\"answer_list\">";
         
         # Output the answer fields....
         while ($myrow2 = mysql_fetch_array($result2))
         {	
                 $id = $myrow2["id"];
                 printf("<li id=\"item_%s\">\n",$id);
                 printf("<input name=\"answer_text_%s\" type=\"text\" value=\"%s\" size=\"28\" onchange=\"javascript:updateQuestion()\" />\n",
                        $myrow2["ans_id"],stripslashes($myrow2["answer_text"]));
                 printf("<label><select value=\"%s\" name=\"link_%s\" onchange=\"javascript:updateQuestion()\">\n"
                        ,$myrow2["question_id"],$myrow2["ans_id"]);
                 # output currently linked to questions
                 $next_question_id = $myrow2["next_question_id"];
                 $result4 = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id AND id = $next_question_id")
                           or exit;
                  $myrow4 = mysql_fetch_array($result4);
                  $next_question_id_val = $myrow4["question_id"];
                 printf("<option value=\"%s\">%s</option>\n",$myrow2["next_question_id"],$next_question_id_val);   #### Need to query Question table to find out ID of next_question_id field.
                 print("<option value=\"---\">---</option>");
                   
                 #output potential questions to link to in drop down box, including currently linked to question
                 $result3 = mysql_query("SELECT * FROM Questions WHERE area_id = $area_id ORDER BY question_id")
                         or exit();
                 while ($myrow3 = mysql_fetch_array($result3))
                 {
                         printf("<option value=\"%s\">%s</option>",$myrow3["id"],$myrow3["question_id"]);
                 }
                 print("</select></label>\n");
                 printf("<label><input type=\"button\" value=\"Delete\" onclick=\"javascript:deleteAnswer(%s,%s,%s); \" />
                        </label><br />\n",$myrow2["ans_id"],$area_id,$question_id);
                 print "</li>\n\n";
         }
         
         print "</ul>";
}

	/**
	* Clean the table carry_forward_temp in the database
	*/

function purge_carry_forward_temp()
{
       # Clean out any items that are more than a day old.  
         $month = str_pad(date("m") - 1, 2, "0", STR_PAD_LEFT);
         $best_before = date("Y") . "-" . $month . "-" . date("d");
         
         $sql = "DELETE FROM carry_forward_temp WHERE session_date > $best_before";
         mysql_query($sql)
                  or die(mysql_error);
}

	/**
	* Return allowed languages
	* @return array list of languages
	*/
function allowed_languages()
{
  $allowed_langs = array ('en', 'fr', 'de', 'pt','ch','hi','ru','ua','be');
	return $allowed_langs;
}
?>
