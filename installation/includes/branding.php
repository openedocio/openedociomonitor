<?php

# Get data
$appName = $_GET["appName"];
$logo = $_GET["logo"];
$icon = $_GET["icon"];
$siteWidth = $_GET["width"];
$topText = $_GET["topText"];
$language = $_GET["language"];
$help_text = $_GET["help_text"];

if($help_text==1) $help_text = "true"; else $help_text = "false";

$topText = str_replace('"','\\\"',$topText);

# form text to write
$text = "<?php \n\n" .
		"##############################################################################\n" .
		"# This is the installation file generated from the install wizard \n" .
		"# If something as gone wrong and you need to set parameters manually, \n" .
		"# you should better edit branding_inc-default.php (which as a valid syntax) \n" .
		"# and rename it to branding_inc.php. \n" . 
		"##############################################################################\n\n" .
		"# This is put in title bar of web browser\n" .
		"\$application_name = '$appName';\n\n" .
		"# Icon to display in browser\n" .
		"\$shortcut = \"$icon\";\n\n" .
		"# Logo to display\n" .
		"\$logo_lg = \"$logo\";\n\n" .
		"# Header text\n" .
		"\$top_text = \"$topText\";\n\n" .
		"# Help text\n" .
		"\$settings[help_text] = \"$help_text\";\n\n" .
		"?>";

# result to pass to callback function
$result = '';

# open noncore/branding_inc.php et set size to 0
$f = "../../noncore/branding_inc.php";
$handle = fopen($f, "w+");

# write data to branding_inc.php
if (is_writable($f)) {
    if (fwrite($handle, $text) === FALSE) {
		$result = 'An error occured while writting data in ' . $fp;
    } else {   
	    $result = 1;   
		fclose($handle);
		# chmod it to 777
		@chmod ($f, 0777);
	}               
}
else {
    $result = $f . 'is not writeable';
} 

if($siteWidth!="")
 {
	$file = file_get_contents("../../noncore/config_inc.php");
	$file = str_replace("\$settings[site_width] = '100%';","\$settings[site_width] = '".$siteWidth."';",$file);
	$file = str_replace("\$settings[default_lang] = 'en';","\$settings[default_lang] = '".$language."';",$file);


	file_put_contents("../../noncore/config_inc.php",$file);
 }

# return the result	
echo $result;

?>