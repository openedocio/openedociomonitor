<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","UTF-8");
define("_HOME","Пачатак");
define("_FAQ","Інструкцыі");
define("_ABOUT","Аб нас");
define("_ADMIN","Уваход інструктара");
define("_AREASEXPERTISE","Часткі");
define("_SPONSORS","Спонсары");
define("_FOLLOWUP","Падрабязна");
define("_FAQLONG","Часта задаюць пытанні");
define("_QUESTION","Пытанне");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">Назад</a> ]");
define("_YOU_APPORXIMATELY","Вы скончылі");
define("_YOU_APPORXIMATELY_COMPLETE","прыкладна");
define("_START_OVER","Пачаць спачатку");
define("_ITEMS_TO_FOLLOW_UP_ON","Паглядзець больш падрабязна");
define("_EMAIL_OR_PRINT","Адправіць на Email або раздрукаваць");
define("_YOUR_STAMP_IS","Ваш код");

define("_INPUT_FINISH_CODE","Калі ласка, увядзіце атрыманы код з папярэдняй часткі");
define("_YOUR_CODE_WRONG","Ваш код няправільны");

define("_FAQ_TEXT","<h1>Кіраўніцтва OpenExpert</h1><p>Прачытайце кожную старонку ці малюнак паказанае вам. Выберыце адно з 5 прапанаваных вам прапаноў, па інфармацыі якую вы ўжо прачыталі.<br /><br /><b>Будзьце ўважлівыя пры выбары правільнага прапановы, таму што калі вы зробіце няправільны выбар, вы будзеце перамешчаныя на некалькі крокаў назад.</b></p>");

define("_PLAY_AUDIO","Праслухаць аўдыё");
define("_PAUSE_AUDIO","Спыніць аўдыё");
define("_INCREASE_VOLUME","Павялічыць гук");
define("_DECREASE_VOLUME","Панізіць гук");

# Admin Text
define("_NEWEXPERTISE","Стварыць новы раздзел");
define("_ADMINHOME","Адміністраванне дадому");
define("_USERS","Карыстальнікі");
define("_SETTINGS","Налады");
define("_ADD","Дадаць");
define("_SAVECHANGES","Захаваць змены");
define("_CARRYFORWARD","Пераход да наступнай старонцы");
define("_NEXTLINK","Наступная спасылка");
define("_DELETE","Выдаліць");
define("_NEWQUESTION","Новае пытанне");
define("_NEWANSWER","Новы адказ");
define("_LINKTO","Спасылка на");
define("_CONFIRMDELETE", "Вы на самой справе хочаце выдаліць падручнік?");
define("_ADMINLOGIN", "Адмін - уваход");
define("_USERNAME", "Імя карыстальніка");
define("_PASSWORD", "Пароль");
define("_PASSWORDERROR", "Імя карыстальніка або пароль не супадаюць");
define("_PROPERTIES","Ўласцівасці");
define("_HIDE","Схаваць");
define("_SHOW","Паказаць");
define("_REALLYDELETE","Пацвердзіць выдаленне?");
define("_EXPORT","Экспарт");
define("_IMPORT","Імпарт падручнікаў");
define("_DOWNLOADFILE","Спампаваць як экспартаваны файл");


# Help Text
define("_NO_HELP_TEXT","Дапамога недаступная");
define("_ADVANCED_HELP_TEXT","Націсніце на элемент для атрымання падказкі");
define("_NEED_HELP","Неабходная дапамога?");
define("_MORE_TOOLS","Дадатковыя інструменты");
define("_HIDE_HELP","Схаваць дапамогу");
define("_NO_HELP_AVAILABLE","Дапамога недаступная для гэтага элемента");
define("_DEFAULT_ADV_HELP_TEXT","Навядзіце на пытанне для атрымання дапамогі");
define("_DEFAULT_ADV_NO_HELP_TEXT","Дапамога недаступная для гэтага адказу");

?>