<?
	/**
	* Pagination
	*/
 class page_view
  {
	/**
	* @var string HTML of Pagination
	*/
	var $result;
	/**
	* @var string Table of database
	*/	
	var $table;
	/**
	* @var integer Rows on page
	*/
	var $on_page;
	/**
	* @var integer Current page
	*/
	var $page;
	/**
	* @var string Where statement for SQL
	*/
	var $where;
	/**
	* @var string Delimiter between page numbers
	*/
	var $delemiter;
	/**
	* @var string Template of url
	*/
	var $link;
	/**
	* @var string Text before Pagination
	*/
	var $content;
	/**
	* @var string Template for current page
	*/
	var $view_page;
	/**
	* @var string Type of count numrows - for GROUP BY
	*/
	var $count_numrows;
	/**
	* @var string Number of rows
	*/
	var $count;
	/**
	* @var integer Max page
	*/
	var $max_page;
	/**
	* @var string HTML to show a previous button
	*/
	var $prev_part;
	/**
	* @var string HTML to show a next button
	*/
	var $next_part;
	/**
	* @var string Template for a previous button
	*/
	var $prev_page;
	/**
	* @var string Template for a next button
	*/
	var $next_page;
	/**
	* @var string Type of template. With controls or without.
	*/
	var $type;
	/**
	* @var integer Current page
	*/
	var $cur_page;
	/**
	* @var string Full query. Execute a full query without auto generation SQL.
	*/
	var $full_query;

	/**
	* Constructor of class
	* @param string $table Table of database
	* @param integer $on_page Rows on page
	* @param integer $page Current page
	* @param string $delemiter Template of url
	* @param string $content Template for current page
	* @param string $view_page Template for current page
	*/

	function __construct($table,$on_page,$page,$delemiter,$content="<br />��������: ",$view_page="<strong>%i%</strong>")
	 {
		$this->on_page = $on_page;
		$this->table = $table;
		$this->page = $page;
		$this->cur_page = $page;
		$this->delemiter = $delemiter;
		$this->content = $content;
		$this->view_page = $view_page;
		$this->type = 'normal';
	 }

	/**
	* Generate links for mobile version
	*/
	function create_mobile()
	 {
		global $mysql;

		 $result = $mysql->Select("SELECT count(*) as count FROM `".$this->table."`".$this->where);

		if($this->count_numrows=="true")
		  $result[0][count] = $result[count];

			if($this->page>1)
			 $form = $form.str_replace("%i%",($this->page-1),$this->prev_part);

			if($this->page*$this->on_page<$result[0][count])
			 $form = $form.str_replace("%i%",($this->page+1),$this->next_part);

			$this->page = ($this->page - 1) * $this->on_page;

		 	  $form = str_replace("_page_1.html",".html",$form);

		$this->result = $form;
	 }

	/**
	* Generate links
	*/
	function create()
 	 {
		global $mysql;

		if($this->where != "") $this->where = " WHERE ".$this->where;

		if($this->full_query!="")
		 $result = $mysql->Select($this->full_query);
		else
		 $result = $mysql->Select("SELECT count(*) as count FROM `".$this->table."`".$this->where);

		if($this->count_numrows=="true")
		  $result[0][count] = $result[count];
		 
		$this->count  = $result[0][count];
		
	$k = 0;

	 $i = 1;
	 $k_end = 100000;

 		$this->max_page = ceil($result[0][count]/$this->on_page);

	if($this->type=="normal")
	 {
		for($i;$i<($result[0][count]/$this->on_page)+1 && $k<$k_end;$i++)
		 {
		$k++;
			 if($i==$this->page)
			$form = $form.str_replace("%i%","$i",$this->view_page);
		 	 else
			$form = $form.str_replace("%i%","$i",$this->link);
	
	
			if($i+1<($result[0][count]/$this->on_page)+1)
			 $form = $form.$this->delemiter;
		 }

		 	if($result[0][count]==0)
		 	  $form = $form.str_replace("%i%","1",$this->view_page);
	 }
	else
	 {
	$list_count = 5;

    $start = ($this->page / $list_count);

  if($start<=1)
   {
   $start = 1;
   $end = $list_count;
   }
  else
   {
	if(ceil($start)/$start != 1) $start = $start + 1;

	   $start = (floor($start)) * $list_count - $list_count + 1;

   $end = $list_count;
   }


  if($start == 0) $start = 1;

 	 $i = $start;
	 $k_end = $end;

			if($start!="1")
			 {
			 $link_next_part = str_replace("%name%","1",$this->next_part);
			 $form = $form.str_replace("%i%","1",$link_next_part);

			 $link_next_part = str_replace("%name%","..",$this->next_part);
			 $form = $form.str_replace("%i%",$i - 1,$link_next_part);
		 	 }

		for($i;$i<($result[0][count]/$this->on_page)+1 && $k<$end;$i++)
		 {
			$k++;

			 if($i==$this->page)
			$form = $form.str_replace("%i%","$i",$this->view_page);
		 	 else
			$form = $form.str_replace("%i%","$i",$this->link);
	
			if($i+1<($result[0][count]/$this->on_page)+1)
			 $form = $form.$this->delemiter;

			if($k==$end && $i+1<=$this->max_page) {
			 $link_next_part = str_replace("%name%","..",$this->next_part);
			 $form = $form.str_replace("%i%",$i + 1,$link_next_part);
			}

			if($k==$end || $i>=($result[0][count]/$this->on_page)+1)
			 if($i+1<$this->max_page) {
			  $link_next_part = str_replace("%name%",$this->max_page,$this->next_part);
			  $form = $form.str_replace("%i%",$this->max_page,$link_next_part);
			 }
		 }

		 	if($result[0][count]==0)
		 	  $form = $form.str_replace("%i%","1",$this->view_page);
	 }	 
		
		 	  $form = str_replace("_page_1.html",".html",$form);

	$value = ($result[0][count]/$this->on_page)+1;

		$this->page = ($this->page - 1) * $this->on_page;

	$this->result = $this->content.$form;
	 }

	/**
	* Generate a previous page
	* @param integer $page Page
	* @param integer $page_disabled Unactive link
	*/
	function prev_page($page,$page_disabled="")
	 {
		if($this->cur_page>1)
		 $this->prev_page = str_replace("%i%",$this->cur_page-1,$page);
		else
		 $this->prev_page = $page_disabled;

		 	  $this->prev_page = str_replace("_page_1.html",".html",$this->prev_page);
	 }

	/**
	* Generate a next page
	* @param integer $page Page
	* @param integer $page_disabled Unactive link
	*/
	function next_page($page,$page_disabled="")
	 {
		if($this->cur_page != $this->max_page && $this->max_page!=0)
		 $this->next_page = str_replace("%i%",$this->cur_page+1,$page);
		else
		 $this->next_page = $page_disabled;

		 	  $this->next_page = str_replace("_page_1.html",".html",$this->next_page);
	 }
  }

?>