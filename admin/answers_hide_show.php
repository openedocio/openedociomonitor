<?php 
include("../config_inc.php");
include("security_inc.php");

$hide_answers = $_GET["hide_answers"];
$area_id = $_GET["area_id"];
$question_id = $_GET["question_id"];

if ($hide_answers == 1) {
    setcookie("hide_answers",1);
} else {
    setcookie("hide_answers",0);
}

header("Location:interview_edit.php?area_id=$area_id&question_id=$question_id#$question_id");

?>