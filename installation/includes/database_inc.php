<?php
# openexpert.sql parse and import function
# taken from PHP Sources : http://www.phpsources.org/scripts373-PHP.htm
# thx to the author !!!
function mysql_import_file($filename, &$errmsg){ 
	# read file
   $lines = file($filename); 

   if(!$lines)  {
      $errmsg = "cannot open file $filename"; 
      return false; 
   } 

   $scriptfile = false; 

   /* Get rid of the comments and form one jumbo line */ 
   foreach($lines as $line)   {
      $line = trim($line); 

      if(!ereg('^--', $line)) {
         $scriptfile.=" ".$line; 
      } 
   } 

   if(!$scriptfile) {
      $errmsg = "no text found in $filename"; 
      return false; 
   } 

   /* Split the jumbo line into smaller lines */ 

   $queries = explode(';-- EOQ --', $scriptfile); 

   /* Run each line as a query */

   foreach($queries as $query) {
      $query = trim($query); 
      if($query == "") { continue; } 
      if(!mysql_query($query.';')) 
      { 
         $errmsg = "query ".$query." failed"; 
         return false; 
      } 
   } 

   // return true if success
   return true; 
}

?>