<?php

/**************************************************************************/
/* This is the language module with all the system messages               */
/*                                                                        */
/* If you made a translation, please go to the site and send to me        */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes     remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/

define("_CHARSET","UTF-8");
define("_HOME","प्रारंभ");
define("_FAQ","निर्देश");
define("_ABOUT","के बारे में");
define("_ADMIN","प्रशिक्षक लॉगइन");
define("_AREASEXPERTISE","इकाइयों");
define("_SPONSORS","प्रायोजक");
define("_FOLLOWUP","पर अनुवर्ती कार्रवाई के लिए आइटम");
define("_FAQLONG","पूछे जाने वाले सवाल");
define("_QUESTION","पेज");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">वापस जाओ</a> ]");
define("_YOU_APPORXIMATELY","आप लगभग रहे हैं");
define("_YOU_APPORXIMATELY_COMPLETE","पूरा");
define("_START_OVER","दुबारा शुरू");
define("_ITEMS_TO_FOLLOW_UP_ON","पर का पालन करने के लिए आइटम");
define("_EMAIL_OR_PRINT","ईमेल या प्रिंट");
define("_YOUR_STAMP_IS","आपका टिकट है");
define("_FAQ_TEXT","<h1>एक ओपन विशेषज्ञ कर रहा है</h1><p>आप के लिए प्रस्तुत के रूप में प्रत्येक पृष्ठ या स्क्रीन पढ़ें. फिर पेश 5 वाक्य से चुनें. तुम सिर्फ पढ़ने पृष्ठ या स्क्रीन में निहित एक का चयन करें.<br /><br /><b>तुम सिर्फ पढ़ने पेज में निहित नहीं एक वाक्य का चयन आप एक या आगे वापस अधिक नहीं भेजेंगे, क्योंकि सही वाक्य का चयन करने के लिए ध्यान रखना.</b></p>");
define("_HEADER_TITLE","<h2>यहाँ शीर्षक दर्ज</h2><h3>यहाँ लेखक दर्ज</h3>");

define("_INPUT_FINISH_CODE","पिछले एक इकाई से इनपुट एक खत्म कोड");
define("_YOUR_CODE_WRONG","आपका कोड गलत है!");

define("_PLAY_AUDIO","ऑडियो खेल");
define("_PAUSE_AUDIO","ऑडियो रोकें");
define("_INCREASE_VOLUME","मात्रा में वृद्धि");
define("_DECREASE_VOLUME","मात्रा में कमी");



# Admin Text
define("_NEWEXPERTISE","एक नया अध्याय बनाएँ");
define("_ADMINHOME","एचआर होम");
define("_USERS","उपयोगकर्ता");
define("_SETTINGS","सेटिंग्स");
define("_ADD","जोड़ना");
define("_SAVECHANGES","परिवर्तन सहेजें");
define("_CARRYFORWARD","आगे ले जाना");
define("_NEXTLINK","अगला लिंक");
define("_DELETE","हटाना");
define("_NEWQUESTION","नई Q");
define("_NEWANSWER","नई A");
define("_LINKTO","लिंक");
define("_CONFIRMDELETE", "क्या आप इस testbook इसे हटाना चाहते हैं?");
define("_ADMINLOGIN", "Admin Login");
define("_USERNAME", "User Name");
define("_PASSWORD", "Password");
define("_PASSWORDERROR", "आपके द्वारा दी गई यूज़रनेम और पासवर्ड मेल नहीं खाते.");
define("_PROPERTIES","गुण");
define("_HIDE","छिपाना");
define("_SHOW","प्रदर्शन");
define("_REALLYDELETE","मिटाएँ?");
define("_EXPORT","निर्यात");
define("_IMPORT","आयात पाठ्यपुस्तकें");
define("_DOWNLOADFILE","निर्यात फ़ाइल के रूप में इस डाउनलोड");


# Help Text
define("_NO_HELP_TEXT","कोई सहायता उपलब्ध नहीं");
define("_ADVANCED_HELP_TEXT","मदद के लिए क्लिक करें");
define("_NEED_HELP","की आवश्यकता है?");
define("_MORE_TOOLS","अधिक टूल्स");
define("_HIDE_HELP","मदद छुपाएं");
define("_NO_HELP_AVAILABLE","इस मद के लिए कोई सहायता उपलब्ध नहीं");
define("_DEFAULT_ADV_HELP_TEXT","इसके बारे में मदद पाने के लिए एक जवाब हॉवर");
define("_DEFAULT_ADV_NO_HELP_TEXT","कोई मदद इस उत्तर के लिए उपलब्ध है");

?>
